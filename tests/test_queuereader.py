"""Test postqm.postqueuereader."""

from unittest import TestCase

from postqm import Postqminternalerror, Postqueueempty
from postqm.queuereader import postqueuerecords
from tests.mocks import emptypostqueue, finitepostqueue, infinitepostqueue


def brokenpostqueuehead():
    """Yield a broken approximation of ``postqueue`` output."""
    if False:  # pylint: disable=using-constant-test
        yield '\n'


def brokenpostqueuetail():
    """Yield a broken approximation of ``postqueue`` output."""
    yield 'head\n'
    for ii in range(2):
        yield 'recordhead\n'
        if not ii % 2:
            yield 'recordextra\n'
        yield 'recordtail\n'
        yield '\n'
    yield 'tail1\n'
    yield 'tail2\n'


class Postqueuerecords_test(TestCase):
    def test_skiphead(self):
        empty = postqueuerecords(emptypostqueue())
        finite = postqueuerecords(finitepostqueue())
        infinite = postqueuerecords(infinitepostqueue())
        broken = postqueuerecords(brokenpostqueuetail())
        with self.assertRaises(Postqueueempty):
            list(empty)
        self.assertSequenceEqual(next(finite),
                                 ['recordhead',
                                  'recordextra',
                                  'recordtail'])
        self.assertSequenceEqual(next(infinite),
                                 ['recordhead',
                                  'recordtail'])
        self.assertSequenceEqual(next(broken),
                                 ['recordhead',
                                  'recordextra',
                                  'recordtail'])

    def test_iteration(self):
        finite = postqueuerecords(finitepostqueue())
        infinite = postqueuerecords(infinitepostqueue())
        broken = postqueuerecords(brokenpostqueuetail())
        for ii in range(20):
            record = next(finite)
            self.assertEqual(record[0], 'recordhead')
            if not ii % 3:
                self.assertEqual(record[1], 'recordextra')
            self.assertEqual(record[-1], 'recordtail')
        for ii in range(20):
            record = next(infinite)
            self.assertSequenceEqual(record,
                                     ['recordhead', 'recordtail'])
        for ii in range(2):
            record = next(broken)
            self.assertEqual(record[0], 'recordhead')
            if not ii % 2:
                self.assertEqual(record[1], 'recordextra')
            self.assertEqual(record[-1], 'recordtail')

    def test_skiptail(self):
        qrecords = list(postqueuerecords(finitepostqueue()))
        self.assertEqual(len(qrecords), 20)
        for record in qrecords:
            self.assertEqual(record[0], 'recordhead')
            self.assertEqual(record[-1], 'recordtail')

    def test_failonbrokenhead(self):
        qrecords = postqueuerecords(brokenpostqueuehead())
        with self.assertRaises(Postqminternalerror):
            next(qrecords)

    def test_failonbrokentail(self):
        qrecords = postqueuerecords(brokenpostqueuetail())
        self.assertSequenceEqual(next(qrecords),
                                 ['recordhead',
                                  'recordextra',
                                  'recordtail'])
        self.assertSequenceEqual(next(qrecords),
                                 ['recordhead', 'recordtail'])
        with self.assertRaises(Postqminternalerror):
            next(qrecords)


# local variables:
# ispell-local-dictionary: "british";
# end:
