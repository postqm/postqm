"""Test postqm.postqm."""

# References:
#
# * Rfc 2606 - Reserved top level Dns names.
#
# * Rfc 5737 - Ipv4 address blocks reserved for documentation.
#
# * Rfc 3849 - Ipv6 address prefix reserved for documentation.


import os
import signal
import sys
from argparse import Namespace
from itertools import islice, tee
from locale import getpreferredencoding
from subprocess import PIPE, STDOUT
from unittest import TestCase
from unittest.mock import ANY, Mock, call, patch

from postqm.postqm import (Entryfilter,
                           Entryfilterlist,
                           Invertedentryfilter,
                           Postqueueempty,
                           Postqueueerror,
                           Qentry,
                           Qidfeeder,
                           Qmessage,
                           Returncodes,
                           Stderraccumulator,
                           Stderrlogger,
                           applyqoperation,
                           cmdhold,
                           cmdls,
                           cmdrelease,
                           cmdrequeue,
                           cmdrm,
                           cmdtop,
                           months,
                           parseargs,
                           postqqids,
                           readpostqueue,
                           runpostqueue,
                           weekdays)
from tests.iterutils import teefirstcontrol
from tests.mocks import (Printthenepipemock,
                         emptypostqueue,
                         exampledates,
                         examplemessage,
                         exampleqentries,
                         finitepostqueue,
                         infinitepostqueue,
                         infiniteqids,
                         mockpopen,
                         patchpopen,
                         repeatexampleqentries)

preferredencoding = getpreferredencoding()


class Months_test(TestCase):
    def test_plain(self):
        self.assertEqual(months['jan'], 1)
        self.assertEqual(months['feb'], 2)
        self.assertEqual(months['dec'], 12)


class Weekdays_test(TestCase):
    def test_plain(self):
        self.assertEqual(weekdays['mon'], 1)
        self.assertEqual(weekdays['tue'], 2)
        self.assertEqual(weekdays['sun'], 7)


class Returncodes_test(TestCase):
    def test_defined(self):
        self.assertEqual(Returncodes.lookup(os.EX_DATAERR),
                         'EX_DATAERR')
        self.assertEqual(Returncodes.lookup(-signal.SIGPIPE),
                         'SIGPIPE')

    def test_undefined(self):
        self.assertEqual(Returncodes.lookup(17),
                         17)


class Stderraccumulator_test(TestCase):
    def test_plain(self):
        popenmock = mockpopen([], ["No such file\n"],
                              os.EX_UNAVAILABLE)
        catproc = popenmock('postcat -q'.split(), stdout=PIPE, stderr=PIPE)
        erraccumulator = Stderraccumulator(catproc)
        erraccumulator.start()
        self.assertEqual(erraccumulator.join(),
                         ["No such file"])


class Qmessage_test(TestCase):
    def test_plain(self):
        qid = "C6F6D38014E"
        with patchpopen('postqm.postqm.Popen',
                        examplemessage(), [], -signal.SIGPIPE) as popenmock:
            qmessage = Qmessage(qid)
        self.assertTrue(qmessage.retrieveresult)
        popenmock.assert_called_once_with(['postcat', '-q', qid],
                                          stdout=PIPE, stderr=PIPE)
        self.assertEqual(qmessage.qid, qid)
        self.assertEqual(qmessage.envelope['sender'], [""])
        self.assertEqual(qmessage.envelope['recipient'],
                         ["info@printinghouse.example.com"])
        self.assertEqual(qmessage.getheader('subject'),
                         ["Undelivered Mail Returned to Sender"])
        self.assertEqual(qmessage.getheader('auto-submitted'),
                         ["auto-replied"])

    def test_readbody(self):
        qid = "C6F6D38014E"
        with patchpopen('postqm.postqm.Popen',
                        examplemessage(), [], -signal.SIGPIPE) as popenmock:
            qmessage = Qmessage(qid, readbody=True)
        self.assertTrue(qmessage.retrieveresult)
        popenmock.assert_called_once_with(['postcat', '-q', qid],
                                          stdout=PIPE, stderr=PIPE)
        self.assertEqual(qmessage.qid, qid)
        self.assertTrue(
            "For further assistance, please send mail to postmaster."
            in qmessage.body)


class Qentry_test(TestCase):
    def test_domainforaddress(self):
        self.assertEqual(Qentry.domainforaddress("local"), None)
        self.assertEqual(Qentry.domainforaddress("local@example.net"),
                         "example.net")

    def test_active(self):
        qentries = exampleqentries()
        self.assertTrue(
            qentries['active11daysoldfrompostmastertospammer1'].active)
        self.assertFalse(
            qentries['12daysoldfrommailerdaemontospammer2'].active)
        self.assertFalse(
            qentries['held3weeksoldfrommailerdaemontospammer2'].active)

    def test_held(self):
        qentries = exampleqentries()
        self.assertFalse(
            qentries['active11daysoldfrompostmastertospammer1'].held)
        self.assertFalse(
            qentries['12daysoldfrommailerdaemontospammer2'].held)
        self.assertTrue(
            qentries['held3weeksoldfrommailerdaemontospammer2'].held)

    def test_size(self):
        qentries = exampleqentries()
        self.assertEqual(
            qentries['active11daysoldfrompostmastertospammer1'].size,
            4923)
        self.assertEqual(
            qentries['12daysoldfrommailerdaemontospammer2'].size,
            22834)
        self.assertEqual(
            qentries['held3weeksoldfrommailerdaemontospammer2'].size,
            12388)

    def test_sender(self):
        qentries = exampleqentries()
        self.assertEqual(
            qentries['active11daysoldfrompostmastertospammer1'].sender,
            "postmaster@example.net")
        self.assertEqual(
            qentries['12daysoldfrommailerdaemontospammer2'].sender,
            "mailer-daemon")
        self.assertEqual(
            qentries['held3weeksoldfrommailerdaemontospammer2'].sender,
            "mailer-daemon")

    def test_senderdomain(self):
        qentries = exampleqentries()
        self.assertEqual(
            qentries['active11daysoldfrompostmastertospammer1'].senderdomain,
            "example.net")
        self.assertEqual(
            qentries['12daysoldfrommailerdaemontospammer2'].senderdomain,
            None)

    def test_recipient(self):
        qentries = exampleqentries()
        self.assertEqual(
            qentries['active11daysoldfrompostmastertospammer1'].recipient,
            ["spammer@e1.example.com"])
        self.assertEqual(
            qentries['12daysoldfrommailerdaemontospammer2'].recipient,
            ["spammer@e2.example.com"])
        self.assertEqual(
            qentries['held3weeksoldfrommailerdaemontospammer2'].recipient,
            ["spammera@e2.example.com", "spammerb@e2.example.com"])

    def test_recipients(self):
        qentries = exampleqentries()
        self.assertEqual(
            qentries['active11daysoldfrompostmastertospammer1'].recipients,
            1)
        self.assertEqual(
            qentries['held3weeksoldfrommailerdaemontospammer2'].recipients,
            2)

    def test_recipientdomain(self):
        qentries = exampleqentries()
        self.assertEqual(
            qentries['active11daysoldfrompostmastertospammer1'].recipientdomain,
            ["e1.example.com"])
        self.assertEqual(
            qentries['12daysoldfrommailerdaemontospammer2'].recipientdomain,
            ["e2.example.com"])

    def test_qdate(self):
        dates = exampledates()
        qentries = exampleqentries()
        self.assertEqual(
            qentries['active11daysoldfrompostmastertospammer1'].qdate,
            dates['elevendaysago'])
        self.assertEqual(
            qentries['12daysoldfrommailerdaemontospammer2'].qdate,
            dates['twelvedaysago'])
        self.assertEqual(
            qentries['held3weeksoldfrommailerdaemontospammer2'].qdate,
            dates['threeweeksago'])

    def test_qage(self):
        qentries = exampleqentries()
        self.assertEqual(
            qentries['active11daysoldfrompostmastertospammer1'].qage,
            11)
        self.assertEqual(
            qentries['12daysoldfrommailerdaemontospammer2'].qage,
            12)
        self.assertEqual(
            qentries['held3weeksoldfrommailerdaemontospammer2'].qage,
            21)

    def test_str(self):
        dates = exampledates()
        qentries = exampleqentries()
        self.assertEqual(
            str(qentries['active11daysoldfrompostmastertospammer1']),
            f"636C828180F*   4923    1  {dates['elevendaysago']}  postmaster@example.net\n"
            "    spammer@e1.example.com\n"
            "    subject: ['Wafers spam']")
        self.assertEqual(
            str(qentries['12daysoldfrommailerdaemontospammer2']),
            f"7B83F281857   22834    1  {dates['twelvedaysago']}  mailer-daemon\n"
            "    spammer@e2.example.com\n"
            "    subject: ['Conference spam']")
        self.assertEqual(
            str(qentries['held3weeksoldfrommailerdaemontospammer2']),
            f"F01656B6153!  12388    2  {dates['threeweeksago']}  mailer-daemon\n"
            "    spammera@e2.example.com\n"
            "    spammerb@e2.example.com\n"
            "    subject: ['Process spam']")
        del qentries['active11daysoldfrompostmastertospammer1'].headers['subject']
        del qentries['12daysoldfrommailerdaemontospammer2'].headers['subject']
        del qentries['held3weeksoldfrommailerdaemontospammer2'].headers['subject']
        self.assertEqual(
            str(qentries['active11daysoldfrompostmastertospammer1']),
            f"636C828180F*   4923    1  {dates['elevendaysago']}  postmaster@example.net\n"
            "    spammer@e1.example.com")
        self.assertEqual(
            str(qentries['12daysoldfrommailerdaemontospammer2']),
            f"7B83F281857   22834    1  {dates['twelvedaysago']}  mailer-daemon\n"
            "    spammer@e2.example.com")
        self.assertEqual(
            str(qentries['held3weeksoldfrommailerdaemontospammer2']),
            f"F01656B6153!  12388    2  {dates['threeweeksago']}  mailer-daemon\n"
            "    spammera@e2.example.com\n"
            "    spammerb@e2.example.com")

    def test_getoutputheader(self):
        qentries = exampleqentries()
        self.assertEqual(
            qentries['active11daysoldfrompostmastertospammer1'].getoutputheader(),
            "-------------------------------------------------------\n"
            "--------QID  --SIZE  NUM  YYYY-MM-DD  SENDER (envelope)\n"
            "    RECIPIENTS (envelope)\n"
            "    subject: SUBJECT (header)")
        del qentries['active11daysoldfrompostmastertospammer1'].headers['subject']
        self.assertEqual(
            qentries['active11daysoldfrompostmastertospammer1'].getoutputheader(),
            "-------------------------------------------------------\n"
            "--------QID  --SIZE  NUM  YYYY-MM-DD  SENDER (envelope)\n"
            "    RECIPIENTS (envelope)")


class Entryfilter_test(TestCase):
    """Test Entryfilter and Invertedentryfilter."""

    def test_queueeq(self):
        qentries = exampleqentries()
        entryfilter1 = Entryfilter(*"sender eq mailer-daemon".split())
        self.assertFalse(entryfilter1(
            qentries['active11daysoldfrompostmastertospammer1']))
        self.assertTrue(entryfilter1(
            qentries['12daysoldfrommailerdaemontospammer2']))
        self.assertTrue(entryfilter1(
            qentries['held3weeksoldfrommailerdaemontospammer2']))
        entryfilter2 = Invertedentryfilter(*"sender eq mailer-daemon".split())
        self.assertTrue(entryfilter2(
            qentries['active11daysoldfrompostmastertospammer1']))
        self.assertFalse(entryfilter2(
            qentries['12daysoldfrommailerdaemontospammer2']))
        self.assertFalse(entryfilter2(
            qentries['held3weeksoldfrommailerdaemontospammer2']))

    def test_queuerx(self):
        qentries = exampleqentries()
        entryfilter1 = Entryfilter(
            *"recipientdomain rx e1[.]example[.]com".split())
        self.assertTrue(entryfilter1(
            qentries['active11daysoldfrompostmastertospammer1']))
        self.assertFalse(entryfilter1(
            qentries['12daysoldfrommailerdaemontospammer2']))
        self.assertFalse(entryfilter1(
            qentries['held3weeksoldfrommailerdaemontospammer2']))
        entryfilter2 = Invertedentryfilter(
            *"recipientdomain rx e1[.]example[.]com".split())
        self.assertFalse(entryfilter2(
            qentries['active11daysoldfrompostmastertospammer1']))
        self.assertTrue(entryfilter2(
            qentries['12daysoldfrommailerdaemontospammer2']))
        self.assertTrue(entryfilter2(
            qentries['held3weeksoldfrommailerdaemontospammer2']))

    def test_queuelt(self):
        qentries = exampleqentries()
        entryfilter1 = Entryfilter(*"qage lt 12".split())
        self.assertTrue(entryfilter1(
            qentries['active11daysoldfrompostmastertospammer1']))
        self.assertFalse(entryfilter1(
            qentries['12daysoldfrommailerdaemontospammer2']))
        self.assertFalse(entryfilter1(
            qentries['held3weeksoldfrommailerdaemontospammer2']))
        entryfilter2 = Invertedentryfilter(*"qage lt 12".split())
        self.assertFalse(entryfilter2(
            qentries['active11daysoldfrompostmastertospammer1']))
        self.assertTrue(entryfilter2(
            qentries['12daysoldfrommailerdaemontospammer2']))
        self.assertTrue(entryfilter2(
            qentries['held3weeksoldfrommailerdaemontospammer2']))

    def test_queuegt(self):
        qentries = exampleqentries()
        entryfilter1 = Entryfilter(*"qage gt 20".split())
        self.assertFalse(entryfilter1(
            qentries['active11daysoldfrompostmastertospammer1']))
        self.assertFalse(entryfilter1(
            qentries['12daysoldfrommailerdaemontospammer2']))
        self.assertTrue(entryfilter1(
            qentries['held3weeksoldfrommailerdaemontospammer2']))
        entryfilter2 = Invertedentryfilter(*"qage gt 20".split())
        self.assertTrue(entryfilter2(
            qentries['active11daysoldfrompostmastertospammer1']))
        self.assertTrue(entryfilter2(
            qentries['12daysoldfrommailerdaemontospammer2']))
        self.assertFalse(entryfilter2(
            qentries['held3weeksoldfrommailerdaemontospammer2']))

    def test_headereq(self):
        qentries = exampleqentries()
        entryfilter1 = Entryfilter("subject", "eq", "Wafers spam")
        self.assertTrue(entryfilter1(
            qentries['active11daysoldfrompostmastertospammer1']))
        self.assertFalse(entryfilter1(
            qentries['12daysoldfrommailerdaemontospammer2']))
        self.assertFalse(entryfilter1(
            qentries['held3weeksoldfrommailerdaemontospammer2']))

    def test_headerrx(self):
        qentries = exampleqentries()
        entryfilter1 = Entryfilter(*"subject rx .*s.spam".split())
        self.assertTrue(entryfilter1(
            qentries['active11daysoldfrompostmastertospammer1']))
        self.assertFalse(entryfilter1(
            qentries['12daysoldfrommailerdaemontospammer2']))
        self.assertTrue(entryfilter1(
            qentries['held3weeksoldfrommailerdaemontospammer2']))


class Stderrlogger_test(TestCase):
    def test_plain(self):
        popenmock = mockpopen([], ["unavailable\n", "incapable\n"],
                              os.EX_UNAVAILABLE)
        qproc = popenmock('postqueue -p'.split(), stdout=PIPE, stderr=PIPE)
        errlogger = Stderrlogger(qproc)
        with self.assertLogs(logger='postqm', level='ERROR') as logs:
            errlogger.start()
            errlogger.join()
        self.assertEqual(logs.output,
                         ["ERROR:postqm.postqm:postqueue"
                          " stderr: unavailable",
                          "ERROR:postqm.postqm:postqueue"
                          " stderr: incapable"])


class Qidfeeder_test(TestCase):
    def test_finite(self):
        popenmock = mockpopen([], [], os.EX_OK)
        supproc = popenmock('postsup -f'.split(),
                            stdin=PIPE, stdout=PIPE, stderr=STDOUT)
        qids, qidrecord = tee([qentry.qid
                               for qentry
                               in exampleqentries().values()])
        qidfeeder = Qidfeeder(supproc, qids)
        qidfeeder.start()
        qidfeeder.join()
        supproc.stdin.write.assert_has_calls([
            call(f"{qid}\n".encode(preferredencoding))
            for qid
            in qidrecord])
        self.assertEqual(supproc.stdin.write.call_count,
                         len(exampleqentries()))
        supproc.stdin.close.assert_called_once_with()

    def test_infinite(self):
        numentries = 100
        popenmock = mockpopen([], [], os.EX_OK, permittedwrites=numentries)
        supproc = popenmock('postsup -f'.split(),
                            stdin=PIPE, stdout=PIPE, stderr=STDOUT)
        qids = infiniteqids()
        qidfeeder = Qidfeeder(supproc, qids, name="infinite")
        with self.assertRaises(OSError), \
             self.assertLogs(logger='postqm', level='ERROR') as logs:
            qidfeeder.start()
            qidfeeder.join()
        self.assertEqual(logs.output,
                         ["ERROR:postqm.propagatingthreading:"
                          "Caught BrokenPipeError:"
                          ' "[Errno 32] Processed enough mock calls."'
                          " in Qidfeeder (infinite)."])
        self.assertEqual(supproc.stdin.write.call_count, numentries)
        supproc.stdin.close.assert_called_once_with()


class Runpostqueue_test(TestCase):
    @staticmethod
    def checkpopencall(popenmock):
        """Check that *popenmock* was called.

        ... with appropriate parameters for ``runpostqueue``."""
        popenmock.assert_called_once_with(
            ['postqueue', '-p'], stdout=PIPE, stderr=PIPE)

    def test_unavailable(self):
        # This is not authentic postqueue output
        with patchpopen('postqm.postqm.Popen', [], ["unavailable\n"],
                        os.EX_UNAVAILABLE) as popenmock, \
             self.assertRaises(Postqueueerror), \
             self.assertLogs(logger='postqm', level='ERROR') as logs:
            next(runpostqueue())
        self.checkpopencall(popenmock)
        self.assertEqual(logs.output,
                         ["ERROR:postqm.postqm:postqueue"
                          " stderr: unavailable"])

    def test_empty(self):
        with patchpopen('postqm.postqm.Popen', emptypostqueue(), [],
                        os.EX_OK) as popenmock:
            self.assertEqual(list(runpostqueue()),
                             ["Mail queue is empty"])
        self.checkpopencall(popenmock)

    def test_finite(self):
        with patchpopen('postqm.postqm.Popen', finitepostqueue(), [],
                        os.EX_OK) as popenmock:
            self.assertEqual(list(runpostqueue()),
                             ["head",
                              "recordhead", "recordextra", "recordtail", "",
                              "recordhead", "recordtail", "",
                              "recordhead", "recordtail", "",
                              "recordhead", "recordextra", "recordtail", "",
                              "recordhead", "recordtail", "",
                              "recordhead", "recordtail", "",
                              "recordhead", "recordextra", "recordtail", "",
                              "recordhead", "recordtail", "",
                              "recordhead", "recordtail", "",
                              "recordhead", "recordextra", "recordtail", "",
                              "recordhead", "recordtail", "",
                              "recordhead", "recordtail", "",
                              "recordhead", "recordextra", "recordtail", "",
                              "recordhead", "recordtail", "",
                              "recordhead", "recordtail", "",
                              "recordhead", "recordextra", "recordtail", "",
                              "recordhead", "recordtail", "",
                              "recordhead", "recordtail", "",
                              "recordhead", "recordextra", "recordtail", "",
                              "recordhead", "recordtail", "",
                              "tail"])
        self.checkpopencall(popenmock)

    def test_infinite(self):
        with patchpopen('postqm.postqm.Popen', infinitepostqueue(), [],
                        os.EX_OK) as popenmock:
            generator = runpostqueue()
            self.assertEqual(next(generator), "head")
        for _ii in range(20):
            self.assertEqual(next(generator), "recordhead")
            self.assertEqual(next(generator), "recordtail")
            self.assertEqual(next(generator), "")
        self.checkpopencall(popenmock)


def patchrunpostqueue(qlines):
    """Simulate ``runpostqueue``."""
    return patch('postqm.postqm.runpostqueue', return_value=iter(qlines))


class Readpostqueue_test(TestCase):
    def setUp(self):
        self.qlines = [
            "-Queue ID- --Size-- ----Arrival Time---- -Sender/Recipient-------\n",
            "27A63806BA*   25464 Sat Apr 27 14:03:32  MAILER-DAEMON\n",
            "                                         bounces+708577-7755-userid=dept.example.net@delivery6.example.com\n",
            "\n",
            "9F74580B7E*   24625 Fri Apr 26 18:12:51  MAILER-DAEMON\n",
            "                                         bounces-760264118253964754@ideas.example.com\n",
            "\n",
            "F0CA580643     8469 Fri Apr 26 08:34:32  MAILER-DAEMON\n",
            "(connect to mail1.example.com[203.0.113.234]:25: Connection timed out)\n",
            "                                         nmn@mail1.example.com\n",
            "\n",
            "-- 57 Kbytes in 3 Requests.\n"]

        self.rdrxideas = Entryfilterlist()
        self.rdrxideas.addfilter(
            Entryfilter(*"recipientdomain rx ideas".split()))
        self.sendereqmailerdaemon = Entryfilterlist()
        self.sendereqmailerdaemon.addfilter(
            Entryfilter(*"sender eq mailer-daemon".split()))

    def test_empty(self):
        with patchrunpostqueue(["Mail queue is empty"]) as runpostqueuemock, \
             self.assertRaises(Postqueueempty):
            next(readpostqueue(self.rdrxideas))
        runpostqueuemock.assert_called_once_with()

    def test_finitequeuefieldsonlyideas(self):
        with patchrunpostqueue(self.qlines) as runpostqueuemock:
            postq = readpostqueue(self.rdrxideas)
            self.assertEqual(len(list(postq)), 1)
        runpostqueuemock.assert_called_once_with()

    def test_finitequeuefieldsonlymailerd(self):
        with patchrunpostqueue(self.qlines) as runpostqueuemock:
            postq = readpostqueue(self.sendereqmailerdaemon)
            self.assertEqual(len(list(postq)), 3)
        runpostqueuemock.assert_called_once_with()


class Readbrokenpostqueue_test(TestCase):
    def setUp(self):
        self.qlines = [
            "-Queue ID- --Size-- ----Arrival Time---- -Sender/Recipient-------\n",
            "\n",  # unreadable queue entry
            "\n",
            "27A63806BA*   25464 Sat Apr 27 14:03:32  MAILER-DAEMON\n",
            "                                         bounces+708577-7755-userid=dept.example.net@delivery6.example.com\n",
            "\n",  # borked recipient?
            "\n",
            "9F74580B7E*   24625 Fri Apr 26 18:12:51  MAILER-DAEMON\n",
            "                                         bounces-760264118253964754@ideas.example.com\n",
            "\n",
            "F0CA580643     8469 Fri Apr 26 08:34:32  MAILER-DAEMON\n",
            "(connect to mail1.example.com[203.0.113.234]:25: Connection timed out)\n",
            "                                         nmn@mail1.example.com\n",
            "\n",
            "-- 57 Kbytes in 3 Requests.\n"]

        self.rdrxideas = Entryfilterlist()
        self.rdrxideas.addfilter(
            Entryfilter(*"recipientdomain rx ideas".split()))
        self.sendereqmailerdaemon = Entryfilterlist()
        self.sendereqmailerdaemon.addfilter(
            Entryfilter(*"sender eq mailer-daemon".split()))

    def test_queuefieldsonlyideas(self):
        with patchrunpostqueue(self.qlines) as runpostqueuemock, \
             self.assertLogs(logger='postqm', level='WARNING') as logs:
            postq = readpostqueue(self.rdrxideas)
            self.assertEqual(len(list(postq)), 1)
        runpostqueuemock.assert_called_once_with()
        self.assertEqual(len(logs.records), 3)

    def test_queuefieldsonlymailerd(self):
        with patchrunpostqueue(self.qlines) as runpostqueuemock, \
             self.assertLogs(logger='postqm', level='WARNING') as logs:
            postq = readpostqueue(self.sendereqmailerdaemon)
            self.assertEqual(len(list(postq)), 3)
        runpostqueuemock.assert_called_once_with()
        self.assertEqual(len(logs.records), 3)


class Postqqids_test(TestCase):
    def test_finite(self):
        qentries = exampleqentries().values()
        self.assertEqual(set(postqqids(qentries)),
                         {"636C828180F", "7B83F281857", "F01656B6153"})
        with patch('builtins.print') as printmock:
            self.assertEqual(set(postqqids(qentries, onlyheld=True)),
                             {"F01656B6153"})
        printmock.assert_has_calls([call("636C828180F not held, skipped.")])
        printmock.assert_has_calls([call("7B83F281857 not held, skipped.")])

    def test_infinite(self):
        numentries = 100
        qentries = repeatexampleqentries()
        qentriesfortest, qentriesrecord = tee(qentries)
        self.assertEqual(list(islice(postqqids(qentriesfortest), numentries)),
                         list(islice((qentry.qid
                                      for qentry
                                      in qentriesrecord), numentries)))


class Applyqoperation_test(TestCase):
    def test_finite(self):
        qids = {qentry.qid
                for qentry
                in exampleqentries().values()}
        with patch('builtins.print') as printmock, \
             patchpopen('postqm.postqm.Popen',
                        ["postsup: 636C828180F: fudged\n",
                         "postsup: 7B83F281857: fudged\n",
                         "postsup: F01656B6153: fudged\n",
                         "postsup: Fudged: 3 messages\n"],
                        [],
                        os.EX_OK) as popenmock:
            applyqoperation('postsup -f'.split(),
                            qids,
                            "postsup: ", ": fudged",
                            "postsup: Fudged: {} message{}",
                            "fudge", "fudged", "fudged.")
        printmock.assert_called_once_with("3 messages fudged.")
        popenmock.assert_called_once_with(
            ['postsup', '-f'], stdin=PIPE, stdout=PIPE, stderr=STDOUT)
        fudgedqids = {args[0].rstrip()
                      for args, _kwargs
                      in popenmock.return_value.stdin.write.call_args_list}
        self.assertEqual({qid.encode(preferredencoding)
                          for qid
                          in qids}, fudgedqids)

    def test_infinite(self):
        numentries = 100
        # applyqoperation through qidfeeder must exhaust
        # qidsforapplyqop before applyqoperation exhausts timeout and
        # maxtimesempty for qidsformock.
        #
        # qidsformock must time out however because the exception from
        # qidfeeder will not be raised finally before it's joined
        # which is after applyqoperation has exhausted qidsformock.
        qidsforapplyqop, qidsformock, qidsforassert = teefirstcontrol(
            infiniteqids(), 3, timeout=0.01, maxtimesempty=3)
        with patch('builtins.print') as printmock, \
             patchpopen('postqm.postqm.Popen',
                        (f"postsup: {qid}: fudged\n"
                         for qid
                         in qidsformock),
                        [],
                        os.EX_OK,
                        permittedwrites=numentries) as popenmock:
            with self.assertRaises(OSError), \
                 self.assertLogs(logger='postqm', level='ERROR') as logs:
                applyqoperation('postsup -f'.split(),
                                qidsforapplyqop,
                                "postsup: ", ": fudged",
                                "postsup: Fudged: {} message{}",
                                "fudge", "fudged", "fudged.")
        self.assertEqual(len(logs.records), 1)
        printmock.assert_not_called()
        popenmock.assert_called_once_with(
            ['postsup', '-f'], stdin=PIPE, stdout=PIPE, stderr=STDOUT)
        fudgedqids = {args[0].rstrip()
                      for args, _kwargs
                      in popenmock.return_value.stdin.write.call_args_list}
        self.assertEqual(len(fudgedqids), numentries)
        self.assertEqual({qid.encode(preferredencoding)
                          for qid
                          in islice(qidsforassert, numentries)}, fudgedqids)


class Cmdls_test(TestCase):
    def setUp(self):
        self.globalargs1 = Namespace()
        self.globalargs1.entryfilterlist = Entryfilterlist()
        self.globalargs1.commandargs = []

    def test_empty(self):
        with patch.object(sys, 'stderr', Mock()) as stderrmock, \
             patch('postqm.postqm.readpostqueue',
                   side_effect=[Postqueueempty(
                       "Mail queue is empty")]) as readpostqueuemock:
            self.assertEqual(cmdls(self.globalargs1), os.EX_OK)
        self.assertEqual(stderrmock.write.call_args_list,
                         [call("Mail queue is empty"),
                          call("\n")])
        readpostqueuemock.assert_called_once_with(ANY, set())
        args, _kwargs = readpostqueuemock.call_args
        self.assertTrue(isinstance(args[0], Entryfilterlist))

    def test_finite(self):
        qentries = exampleqentries()
        readpostqueuemock = Mock(spec=readpostqueue,
                                 return_value=qentries.values())
        with patch('builtins.print') as printmock, \
             patch('postqm.postqm.readpostqueue', new=readpostqueuemock):
            self.assertEqual(cmdls(self.globalargs1), os.EX_OK)
        self.assertEqual(printmock.call_count, len(qentries) + 2)
        self.assertEqual(
            printmock.call_args_list[-1],
            call(f"Listed {len(qentries):d} messages."))
        readpostqueuemock.assert_called_once_with(ANY, set())
        callargs = readpostqueuemock.call_args
        args, _kwargs = callargs
        self.assertTrue(isinstance(args[0], Entryfilterlist))

    def test_infinite(self):
        # Test this many calls to print():
        numentries = 100
        readpostqueuemock = Mock(spec=readpostqueue,
                                 return_value=repeatexampleqentries())
        with patch('postqm.postqm.readpostqueue', new=readpostqueuemock), \
             patch('builtins.print',
                   new=Printthenepipemock(permittedcalls=numentries)
             ) as printmock, \
             self.assertRaises(OSError):
            self.assertEqual(cmdls(self.globalargs1), os.EX_OK)
        self.assertEqual(printmock.call_count, numentries)
        readpostqueuemock.assert_called_once_with(ANY, set())
        args, _kwargs = readpostqueuemock.call_args
        self.assertTrue(isinstance(args[0], Entryfilterlist))


class Cmdtop_test(TestCase):
    def setUp(self):
        self.globalargs1 = Namespace()
        self.globalargs1.entryfilterlist = Entryfilterlist()
        self.globalargs1.commandargs = []
        self.globalargs2 = Namespace()
        self.globalargs2.entryfilterlist = Entryfilterlist()
        self.globalargs2.commandargs = "sender".split()
        self.globalargs3 = Namespace()
        self.globalargs3.entryfilterlist = Entryfilterlist()
        self.globalargs3.commandargs = "sender qage".split()
        self.globalargs4 = Namespace()
        self.globalargs4.entryfilterlist = Entryfilterlist()
        self.globalargs4.commandargs = "--sum sender size".split()

    def test_empty(self):
        with patch.object(sys, 'stderr', Mock()) as stderrmock, \
             patch('postqm.postqm.readpostqueue',
                   side_effect=[Postqueueempty(
                       "Mail queue is empty")]) as readpostqueuemock:
            self.assertEqual(cmdtop(self.globalargs1), os.EX_OK)
        self.assertEqual(stderrmock.write.call_args_list,
                         [call("Mail queue is empty"),
                          call("\n")])
        readpostqueuemock.assert_called_once_with(ANY, {'subject'})
        args, _kwargs = readpostqueuemock.call_args
        self.assertTrue(isinstance(args[0], Entryfilterlist))

    def test_finiteimplicitsubject(self):
        qentries = exampleqentries()
        readpostqueuemock = Mock(spec=readpostqueue,
                                 return_value=qentries.values())
        with patch('builtins.print') as printmock, \
             patch('postqm.postqm.readpostqueue', new=readpostqueuemock):
            self.assertEqual(cmdtop(self.globalargs1), os.EX_OK)
        printmock.assert_called_once_with(
            "No matching fields with frequency greater than 2 found.")
        readpostqueuemock.assert_called_once_with(ANY, {'subject'})
        args, _kwargs = readpostqueuemock.call_args
        self.assertTrue(isinstance(args[0], Entryfilterlist))
        self.assertEqual(
            printmock.call_args_list,
            [call("No matching fields with frequency greater than 2 found.")])

    def test_finitesender(self):
        qentries = exampleqentries()
        readpostqueuemock = Mock(spec=readpostqueue,
                                 return_value=qentries.values())
        with patch('builtins.print') as printmock, \
             patch('postqm.postqm.readpostqueue', new=readpostqueuemock):
            self.assertEqual(cmdtop(self.globalargs2), os.EX_OK)
        self.assertEqual(printmock.call_count, 1 + 2)
        readpostqueuemock.assert_called_once_with(ANY, set())
        args, _kwargs = readpostqueuemock.call_args
        self.assertTrue(isinstance(args[0], Entryfilterlist))
        self.assertEqual(
            printmock.call_args_list,
            [call("   2 mailer-daemon"),
             call("---- ------"),
             call("FREQ SENDER")])

    def test_finitesenderqage(self):
        qentries = exampleqentries()
        readpostqueuemock = Mock(spec=readpostqueue,
                                 return_value=qentries.values())
        with patch('builtins.print') as printmock, \
             patch('postqm.postqm.readpostqueue', new=readpostqueuemock):
            self.assertEqual(cmdtop(self.globalargs3), os.EX_OK)
        self.assertEqual(printmock.call_count, 1 + 2)
        readpostqueuemock.assert_called_once_with(ANY, set())
        args, _kwargs = readpostqueuemock.call_args
        self.assertTrue(isinstance(args[0], Entryfilterlist))
        self.assertEqual(
            printmock.call_args_list,
            [call("         16.5 mailer-daemon"),
             call("------------- ------"),
             call("AVERAGE(QAGE) SENDER")])

    def test_finitesumsendersize(self):
        qentries = exampleqentries()
        readpostqueuemock = Mock(spec=readpostqueue,
                                 return_value=qentries.values())
        with patch('builtins.print') as printmock, \
             patch('postqm.postqm.readpostqueue', new=readpostqueuemock):
            self.assertEqual(cmdtop(self.globalargs4), os.EX_OK)
        self.assertEqual(printmock.call_count, 1 + 2)
        readpostqueuemock.assert_called_once_with(ANY, set())
        args, _kwargs = readpostqueuemock.call_args
        self.assertTrue(isinstance(args[0], Entryfilterlist))
        self.assertEqual(
            printmock.call_args_list,
            [call("    35222 mailer-daemon"),
             call("--------- ------"),
             call("SUM(SIZE) SENDER")])


class Cmdtestermixin():
    """Template for tests of all cmd* that call ``applyqoperation``."""

    def setUp(self):
        """Prepare parsed arguments."""
        self.qagegttenglobalargs = Namespace()
        self.qagegttenglobalargs.entryfilterlist = Entryfilterlist()
        self.qagegttenglobalargs.entryfilterlist.addfilter(
            Entryfilter(*"qage gt 10".split()))
        self.qagegttenglobalargs.commandargs = []
        self.qagegttendryrunglobalargs = Namespace()
        self.qagegttendryrunglobalargs.entryfilterlist = Entryfilterlist()
        self.qagegttendryrunglobalargs.entryfilterlist.addfilter(
            Entryfilter(*"qage gt 10".split()))
        self.qagegttendryrunglobalargs.commandargs = "--dry-run".split()
        self.qentries = exampleqentries()
        self.repeatedqentries = repeatexampleqentries()

    def test_empty(self):
        """Test ``calltotest`` with empty mail queue."""
        with patch.object(sys, 'stderr', Mock()) as stderrmock, \
             patch('postqm.postqm.applyqoperation') as applyqoperationmock, \
             patch('postqm.postqm.readpostqueue',
                   side_effect=[Postqueueempty(
                       "Mail queue is empty")]) as readpostqueuemock:
            self.assertEqual(self.calltotest(self.qagegttenglobalargs),
                             os.EX_OK)
        self.assertEqual(stderrmock.write.call_args_list,
                         [call("Mail queue is empty"),
                          call("\n")])
        readpostqueuemock.assert_called_once_with(ANY)
        args, _kwargs = readpostqueuemock.call_args
        self.assertTrue(isinstance(args[0], Entryfilterlist))
        applyqoperationmock.assert_not_called()

    def test_dryrun(self):
        """Test ``calltotest`` with dry run in the parameters."""
        readpostqueuemock = Mock(spec=readpostqueue,
                                 return_value=self.qentries.values())
        with patch('builtins.print') as printmock, \
             patch('postqm.postqm.readpostqueue', new=readpostqueuemock), \
             patch('postqm.postqm.applyqoperation') as applyqoperationmock:
            self.assertEqual(
                self.calltotest(self.qagegttendryrunglobalargs),
                os.EX_OK)
        printmock.assert_called_once_with(ANY)
        args, _kwargs = printmock.call_args
        self.assertTrue(args[0].startswith(
            "Would have requested 3 messages to be"))
        self.assertEqual(applyqoperationmock.call_count, 0)
        readpostqueuemock.assert_called_once_with(ANY)
        args, _kwargs = readpostqueuemock.call_args
        self.assertTrue(isinstance(args[0], Entryfilterlist))

    def test_finite(self):
        """Test ``calltotest`` with finite mail queue."""
        readpostqueuemock = Mock(spec=readpostqueue,
                                 return_value=self.qentries.values())
        with patch('postqm.postqm.readpostqueue', new=readpostqueuemock), \
             patch('postqm.postqm.applyqoperation') as applyqoperationmock:
            self.assertEqual(self.calltotest(self.qagegttenglobalargs),
                             os.EX_OK)
        applyqoperationmock.assert_called_once_with(
            self.postsupercalllistbase,
            ANY,
            *(self.applyqopexpectedargs
              + self.applyqopdiagargs))
        args, _kwargs = applyqoperationmock.call_args
        self.assertEqual(len(list(args[1])), 3)
        readpostqueuemock.assert_called_once_with(ANY)
        args, _kwargs = readpostqueuemock.call_args
        self.assertTrue(isinstance(args[0], Entryfilterlist))

    def test_infinite(self):
        """Test ``calltotest`` with infinite mail queue."""
        numentries = 100
        qentriesformock, qentriesrecord = tee(self.repeatedqentries)
        readpostqueuemock = Mock(spec=readpostqueue,
                                 return_value=qentriesformock)
        with patch('postqm.postqm.readpostqueue', new=readpostqueuemock), \
             patch('postqm.postqm.applyqoperation') as applyqoperationmock:
            self.assertEqual(self.calltotest(self.qagegttenglobalargs),
                             os.EX_OK)
        applyqoperationmock.assert_called_once_with(
            self.postsupercalllistbase,
            ANY,
            *(self.applyqopexpectedargs
              + self.applyqopdiagargs))
        args, _kwargs = applyqoperationmock.call_args
        self.assertEqual(list(islice(args[1], numentries)),
                         list(postqqids(islice(qentriesrecord, numentries))))
        readpostqueuemock.assert_called_once_with(ANY)
        args, _kwargs = readpostqueuemock.call_args
        self.assertTrue(isinstance(args[0], Entryfilterlist))


class Cmdrm_test(Cmdtestermixin, TestCase):
    # Attributes specific to cmdrm.
    postsupercalllistbase = ['postsuper', '-d', '-', 'hold']
    applyqopexpectedargs = ["postsuper: ",
                            ": removed",
                            "postsuper: Deleted: {} message{}"]
    applyqopdiagargs = ["delete", "deleted", "DELETED!!"]

    def setUp(self):
        super().setUp()
        self.emptyglobalargs = Namespace()
        self.emptyglobalargs.entryfilterlist = Entryfilterlist()
        self.emptyglobalargs.commandargs = []
        for qentry in self.qentries.values():
            # Rm normally only works on held queue entries so fudge
            # the held flag.
            qentry.held = True
        self.repeatedqentries = repeatexampleqentries(fudgeheld=True)

    @staticmethod
    def calltotest(args):
        """Reference to ``cmdrm``."""
        return cmdrm(args)

    def test_nofilter(self):
        readpostqueuemock = Mock(spec=readpostqueue,
                                 return_value=self.qentries.values())
        with patch('builtins.input', return_value='n') as inputmock, \
             patch('postqm.postqm.readpostqueue', new=readpostqueuemock), \
             patch('postqm.postqm.applyqoperation') as applyqoperationmock:
            self.assertEqual(self.calltotest(self.emptyglobalargs),
                             os.EX_USAGE)
            inputmock.assert_called_once_with(
                "This would delete the entire mail queue.\nConfirm [y/n]: ")
        self.assertEqual(applyqoperationmock.call_count, 0)
        self.assertEqual(readpostqueuemock.call_count, 0)


class Cmdhold_test(Cmdtestermixin, TestCase):
    # Attributes specific to cmdhold.
    postsupercalllistbase = ['postsuper', '-h', '-']
    applyqopexpectedargs = ["postsuper: ",
                            ": placed on hold",
                            "postsuper: Placed on hold: {} message{}"]
    applyqopdiagargs = ["hold", "held", "held."]

    @staticmethod
    def calltotest(args):
        """Reference to ``cmdhold``."""
        return cmdhold(args)


class Cmdrelease_test(Cmdtestermixin, TestCase):
    postsupercalllistbase = ['postsuper', '-H', '-']
    applyqopexpectedargs = ["postsuper: ",
                            ": released from hold",
                            "postsuper: Released from hold: {} message{}"]
    applyqopdiagargs = ["release", "released", "released."]

    @staticmethod
    def calltotest(args):
        """Reference to ``cmdrelease``."""
        return cmdrelease(args)


class Cmdrequeue_test(Cmdtestermixin, TestCase):
    postsupercalllistbase = ['postsuper', '-r', '-']
    applyqopexpectedargs = ["postsuper: ",
                            ": requeued",
                            "postsuper: Requeued: {} message{}"]
    applyqopdiagargs = ["requeue", "requeued", "requeued."]

    @staticmethod
    def calltotest(args):
        """Reference to ``cmdrequeue``."""
        return cmdrequeue(args)


class Parseargs_test(TestCase):
    def test_includelsrecipient(self):
        args = parseargs('-i sender eq mailer-daemon ls recipient'.split())
        self.assertEqual(type(args.entryfilterlist), Entryfilterlist)
        self.assertEqual(len(args.entryfilterlist), 1)
        self.assertEqual(args.command, 'ls')
        self.assertSequenceEqual(args.commandargs, ['recipient'])

    def test_excludelsrecipient(self):
        args = parseargs('-e sender eq mailer-daemon ls recipient'.split())
        self.assertEqual(type(args.entryfilterlist), Entryfilterlist)
        self.assertEqual(len(args.entryfilterlist), 1)
        self.assertEqual(args.command, 'ls')
        self.assertSequenceEqual(args.commandargs, ['recipient'])

    def test_nofilterls(self):
        args = parseargs('ls'.split())
        self.assertEqual(type(args.entryfilterlist), Entryfilterlist)
        self.assertEqual(len(args.entryfilterlist), 0)
        self.assertEqual(args.command, 'ls')
        self.assertSequenceEqual(args.commandargs, [])

    def test_includebadfilter(self):
        with self.assertRaises(SystemExit) as sysexitcm, \
             patch.object(sys, 'stderr', Mock()) as stderrmock:
            parseargs('-i sender nn mailer-daemon ls recipient'.split())
        self.assertEqual(sysexitcm.exception.code, 2)
        self.assertTrue(stderrmock.write.call_count > 0)
        firstwriteargs, _firstwritekwargs = stderrmock.write.call_args_list[0]
        self.assertTrue(firstwriteargs[0].startswith("usage: "))

    def test_version(self):
        with self.assertRaises(SystemExit) as sysexitcm, \
             patch.object(sys, 'stdout', Mock()) as stdoutmock:
            parseargs('--version'.split())
        self.assertEqual(sysexitcm.exception.code, 0)
        self.assertTrue(stdoutmock.write.call_count > 0)
        firstwriteargs, _firstwritekwargs = stdoutmock.write.call_args_list[0]
        self.assertTrue(firstwriteargs[0].startswith("Postqm "))


# local variables:
# ispell-local-dictionary: "british";
# end:
