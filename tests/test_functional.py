"""Test postqm.functional."""

from unittest import TestCase

from postqm.functional import identity


class Identity_test(TestCase):
    def test_plain(self):
        self.assertEqual(5, identity(5))
        self.assertEqual('fudge', identity('fudge'))
        self.assertEqual(['fudge', 5], identity(['fudge', 5]))
        self.assertEqual({'fudge': 5}, identity({'fudge': 5}))


# local variables:
# ispell-local-dictionary: "british";
# end:
