"""Itertools parallels."""

import errno
from collections import deque
from threading import Event


def teefirstcontrol(iterable,
                    numoutputs=2,
                    timeout=None,
                    maxtimesempty=None):
    """Return one independent and *numoutputs* - 1 dependent iterators.

    From a single *iterable*.

    *timeout* limits how long and *maxtimesempty* controls how many
    times to wait in the dependent iterators if there is no element to
    yield (already drawn from the independent iterator) before raising
    ``OSError`` with ``EPIPE``."""
    iterator = iter(iterable)
    deques = [deque() for ii in range(numoutputs)]
    availableitem = Event()

    def gencontrol(localdq):
        while True:
            # When the local deque is empty fetch a new value and load
            # it to all the deques.
            if not localdq:
                try:
                    item = next(iterator)
                except StopIteration:
                    return
                for dq in deques:
                    dq.append(item)
                availableitem.set()
                availableitem.clear()
            yield localdq.popleft()

    def gen(localdq):
        timesempty = 0
        while True:
            # When the local deque is empty wait and then raise epipe.
            if not localdq:
                if (maxtimesempty is not None
                    and timesempty >= maxtimesempty):
                    raise OSError(errno.EPIPE,
                                  f"Drew blank {maxtimesempty} times.")
                timesempty += 1
                availableitem.wait(timeout)
                continue
            timesempty = 0
            yield localdq.popleft()

    return tuple(gencontrol(dq) if num == 0 else gen(dq)
                 for num, dq
                 in enumerate(deques))


# local variables:
# ispell-local-dictionary: "british";
# end:
