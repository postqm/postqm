"""Data for testing, mocks and stubs."""

import errno
from datetime import date, timedelta
from random import randint
from subprocess import Popen
from unittest.mock import DEFAULT, Mock, mock_open, patch

from postqm.postqm import Qentry


class Stdoutstub:
    """Open file stub to use as ``stdout`` when mocking ``Popen``.

    (Or as ``stderr``.) Unlike ``mock_open``s ``read_data``
    ``outputcontainer`` can be an infinite iterator."""

    def __init__(self, outputcontainer):
        """Record *outputcontainer*.

        *outputcontainer* should be an iterator. No splitting will be
        done in this class to avoid exhausting the iterator
        prematurely or entering an infinite loop."""
        self._stuboutputiterable = iter(outputcontainer)

    def __iter__(self):
        """Implement iterator protocol."""
        return self

    def __next__(self):
        """Return next item from ``outputiterable`` if available.

        Otherwise raise ``ValueError``."""
        if self._stuboutputiterable is None:
            raise ValueError("I/O operation on closed file.")
        return next(self._stuboutputiterable).encode('utf-8')

    def close(self):
        """Make sure ``__next__`` will no longer work."""
        self._stuboutputiterable = None


def stdinstub(permittedwrites=None):
    """Open file stub to use as ``stdin`` when mocking ``Popen``."""
    stdin = mock_open()
    if permittedwrites is not None:
        sides = [DEFAULT for ii in range(permittedwrites - 1)]
        sides.append(OSError(errno.EPIPE, "Processed enough mock calls."))
        stdin.return_value.write.side_effect = sides
    return stdin


def mockpopen(output, errout, returncode, permittedwrites=None):
    """Mock ``subprocess.Popen``.

    Return ``popenmock``.

    Parameters *output* and *errout* will be iterated over on
    access to ``procmock.stdout`` and ``procmock.stderr``
    respectively.

    *returncode* will be returned on access to
    ``procmock.returncode``.

    *permittedwrites* limits the number of writes to
    ``procmock.write``.

    ``procmock`` will be available as ``popenmock.return_value``."""
    procmock = Mock(spec=Popen)

    def _popencall(args, *_remainingpargs, **ignored_kwargs):
        """Record *args* and return ``procmock`` to code under test."""
        procmock.args = args
        return procmock

    procmock.returncode = returncode
    procmock.stdin = stdinstub(
        permittedwrites=permittedwrites)('fudge', 'w')
    procmock.stdout = Stdoutstub(output)
    procmock.stderr = Stdoutstub(errout)
    return Mock(spec=Popen,
                side_effect=_popencall,
                return_value=procmock)


def patchpopen(target, output, errout, returncode,
               permittedwrites=None):
    """Patch *target*.

    Dispatch to ``mockpopen`` to create the mock.

    If used as a function decorator the decorated function will
    receive an extra parameter ``popenmock`` which mocks the ``Popen``
    constructor."""
    return patch(target,
                 new_callable=lambda: mockpopen(
                     output, errout, returncode,
                     permittedwrites=permittedwrites))


class Printthenepipemock(Mock):
    """Mock for print() that will raise ``OSError`` after a few calls."""

    def __init__(self, *args, permittedcalls=25, **kwargs):
        """Defer to ``super`` ``__init__`` and record *permittedcalls*."""
        super().__init__(*args, **kwargs)
        self._permittedcalls = permittedcalls

    def __call__(self, *args, **kwargs):
        """Defer to ``super`` ``__call__`` or raise ``OSError``.

        If more than ``permittedcalls`` have been made raise
        ``OSError`` with ``errno.EPIPE``."""
        result = super().__call__(*args, **kwargs)
        if len(self.call_args_list) >= self._permittedcalls:
            raise OSError(errno.EPIPE, "Processed enough mock calls.")
        return result


def examplemessage():
    """Return an example message to feed Qmessage with."""
    with open('tests/postcat-qC6F6D38014E.out', encoding="utf-8") as pcout:
        yield from pcout


def exampledates():
    """Return some example dates."""
    dates = {'elevendaysago': date.today() - timedelta(days=11),
             'twelvedaysago': date.today() - timedelta(days=12),
             'threeweeksago': date.today() - timedelta(weeks=3)}
    return dates


def examplepostqueuerecords():
    """Return some example postqueuerecords."""
    dates = exampledates()
    # pylint: disable=consider-using-f-string
    return {
        'active11daysoldfrompostmastertospammer1':
        ('636C828180F*    4923 {} 00:26:26  postmaster@example.net'.format(
            dates['elevendaysago'].strftime("%a %b %e")),
         '                                         spammer@e1.example.com'),
        '12daysoldfrommailerdaemontospammer2':
        ('7B83F281857    22834 {} 10:53:54  MAILER-DAEMON'.format(
            dates['twelvedaysago'].strftime("%a %b %e")),
         '        (connect to mx.example.com[203.0.113.233]: Connection refused)',
         '                                         spammer@e2.example.com'),
        'held3weeksoldfrommailerdaemontospammer2':
        ('F01656B6153!   12388 {} 00:20:33  MAILER-DAEMON'.format(
            dates['threeweeksago'].strftime("%a %b %e")),
         '                                         spammera@e2.example.com',
         '                                         spammerb@e2.example.com'),
    }


def exampleqentries():
    """Return some example qentries."""
    postqueuerecords = examplepostqueuerecords()
    qentries = {key: Qentry(value)
                for key, value
                in postqueuerecords.items()}
    # Fudge subjects
    qentries['active11daysoldfrompostmastertospammer1'].headers['subject'] = (
        ["Wafers spam"])
    qentries['12daysoldfrommailerdaemontospammer2'].headers['subject'] = (
        ["Conference spam"])
    qentries['held3weeksoldfrommailerdaemontospammer2'].headers['subject'] = (
        ["Process spam"])
    return qentries


def makeqid():
    """Return a random qid."""
    qidint = randint(0, 0xfffffffffff)
    return f"{qidint:011X}"


def infiniteqids():
    """Return random qids."""
    while True:
        yield makeqid()


def repeatexampleqentries(fudgeheld=None):
    """Yield the example qentries interminably.

    Subject headers are not set.

    ``qid`` is generated for each qentry.

    If *fudgeheld* is not None set ``held`` on all yielded qentries to
    *fudgeheld*."""
    postqueuerecords = examplepostqueuerecords().values()
    while True:
        for postqueuerecord in postqueuerecords:
            qentry = Qentry(postqueuerecord)
            qentry.qid = makeqid()
            if fudgeheld is not None:
                qentry.held = fudgeheld
            yield qentry


def emptypostqueue():
    """Yield only "Mail queue is empty".

    With terminating newline."""
    yield "Mail queue is empty\n"


def finitepostqueue():
    """Yield a finite approximation of ``postqueue`` output."""
    yield 'head\n'
    for ii in range(20):
        yield 'recordhead\n'
        if not ii % 3:
            yield 'recordextra\n'
        yield 'recordtail\n'
        yield '\n'
    yield 'tail\n'


def infinitepostqueue():
    """Yield an infinite approximation of ``postqueue`` output."""
    yield 'head\n'
    while True:
        yield 'recordhead\n'
        yield 'recordtail\n'
        yield '\n'


# local variables:
# ispell-local-dictionary: "british";
# end:
