"""Test postqm.cachedproperty."""

from random import random
from unittest import TestCase

from postqm.cachedproperty import cachedproperty


class Staticrandom:
    """Keep two stable but random properties."""

    def __init__(self, base):
        """Store base for return values."""
        self.base = base

    @cachedproperty
    def randoma(self):
        """Return random a."""
        return self.base + random()

    @cachedproperty
    def randomb(self):
        """Return random b."""
        return 2 + self.base + random()


class Cache_test(TestCase):
    def setUp(self):
        self.random0 = Staticrandom(0)
        self.random1 = Staticrandom(1)

    def test_plain(self):
        random0a = self.random0.randoma
        random0b = self.random0.randomb
        random1a = self.random1.randoma
        random1b = self.random1.randomb

        # Repeated evaluation should produce consistent results.
        self.assertEqual(random0a, self.random0.randoma)
        self.assertEqual(random0b, self.random0.randomb)
        self.assertEqual(random1a, self.random1.randoma)
        self.assertEqual(random1b, self.random1.randomb)

        # Different properties should not produce the same results.
        self.assertNotEqual(random0a, random0b)
        self.assertNotEqual(random1a, random1b)

        # Properties on different objects should not produce the same
        # results.
        self.assertNotEqual(random0a, random1a)
        self.assertNotEqual(random0b, random1b)

    def test_doc(self):
        self.assertEqual(Staticrandom.randoma.__doc__, "Return random a.")
        self.assertEqual(Staticrandom.randomb.__doc__, "Return random b.")


# local variables:
# ispell-local-dictionary: "british";
# end:
