"""Test postqm.propagatingthreading."""

from operator import add
from time import sleep
from unittest import TestCase

from postqm.propagatingthreading import Propagatingthread


class Returningthread(Propagatingthread):
    """Thread that returns a value from ``propagatingrun``."""

    def propagatingrun(self):
        return 3


class Raisingthread(Propagatingthread):
    """Thread that raises an exception in ``propagatingrun``."""

    def propagatingrun(self):
        raise KeyError(17)


class Sleepingthread(Propagatingthread):
    """Thread that sleeps before returning from ``propagatingrun``."""

    def propagatingrun(self):
        sleep(0.1)
        return 3


class Propagatingthread_test(TestCase):
    def test_targetreturn(self):
        testthread = Propagatingthread(target=add, args=(1, 2))
        testthread.start()
        self.assertEqual(testthread.join(), 3)

    def test_targetexception(self):
        testthread = Propagatingthread(target={}.pop, args=(17,),
                                       name='targetexception')
        with self.assertRaises(KeyError), \
             self.assertLogs(logger='postqm.propagatingthreading',
                             level='ERROR') as logs:
            testthread.start()
            testthread.join()
        self.assertEqual(logs.output,
                         ['ERROR:postqm.propagatingthreading:'
                          'Caught KeyError: "17" in Propagatingthread'
                          ' (targetexception).'])

    def test_propagatingrunreturn(self):
        testthread = Returningthread()
        testthread.start()
        self.assertEqual(testthread.join(), 3)

    def test_propagatingrunexception(self):
        testthread = Raisingthread(name='propagatingrunexception')
        with self.assertRaises(KeyError), \
             self.assertLogs(logger='postqm.propagatingthreading',
                             level='ERROR') as logs:
            testthread.start()
            testthread.join()
        self.assertEqual(logs.output,
                         ['ERROR:postqm.propagatingthreading:'
                          'Caught KeyError: "17" in Raisingthread'
                          ' (propagatingrunexception).'])

    def test_propagatingrunsleepreturn(self):
        testthread = Sleepingthread()
        testthread.start()
        self.assertEqual(testthread.join(timeout=0.01), None)
        self.assertEqual(testthread.is_alive(), True)
        self.assertEqual(testthread.join(), 3)


# local variables:
# ispell-local-dictionary: "british";
# end:
