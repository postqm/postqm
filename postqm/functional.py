"""Utilities for functional programming."""


def identity(arg):
    """Return *arg* unmodified."""
    return arg


# local variables:
# ispell-local-dictionary: "british";
# end:
