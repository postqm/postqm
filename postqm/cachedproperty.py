"""Cached property decorator."""


class cachedproperty:
    """Decorator for read only properties evaluated only once."""

    def __init__(self, getcallable):
        """Store details about the ``cachedproperty``."""
        self.getcallable = getcallable
        self.__name__ = getcallable.__name__
        self.__doc__ = getcallable.__doc__
        # __init__ itself must return none but the call to it
        # returns the new instance.

    def __get__(self, instance, owner):
        """Evaluate, cache and return the ``cachedproperty``.

        Except when called without *instance*; in that case return self
        so introspection can discover name and documentation.

        The cached value is written to ``__dict__`` on *instance*
        under the same name as the ``cachedproperty``."""
        if instance is None:
            return self
        value = instance.__dict__[self.__name__] = self.getcallable(instance)
        return value


# local variables:
# ispell-local-dictionary: "british";
# end:
