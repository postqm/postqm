"""Process ``postqueue -p`` output."""

import logging

from . import Postqminternalerror, Postqueueempty

logger = logging.getLogger(__name__)


def readrecord(postqueueoutput):
    """Return one record from *postqueueoutput* or None.

    Raise Postqminternalerror if the last item is not the footer."""
    separator = ''
    record = []
    for item in postqueueoutput:
        item = item.rstrip()
        if item == separator:
            if not record:
                # postqueue -p outputs one record per queue file, even
                # when a queue file is so broken that Postfix can
                # learn nothing about it.
                logger.warning(
                    "Ignoring empty record in postqueue -p output")
                continue
            return record
        record.append(item)
    # End of postqueueoutput
    if len(record) != 1:
        raise Postqminternalerror(
            f"Postqueue output does not end with footer: {record}")
    return None


def postqueuerecords(postqueueoutput):
    """Yield records found in *postqueueoutput*.

    If first and only item of *postqueueoutput* says "Mail queue is
    empty" raise Postqueueempty.

    Otherwise discard first item and yield lists of items separated by
    empty strings until a lone item remains and discard that too."""
    try:
        headorempty = next(postqueueoutput).rstrip()
    except StopIteration:
        raise Postqminternalerror("No output from postqueue.") from None
    if headorempty == "Mail queue is empty":
        raise Postqueueempty(headorempty)

    # If we're still here headorempty was the header that should be
    # discarded.
    while True:
        record = readrecord(postqueueoutput)
        if record is not None:
            yield record
            continue
        return


# local variables:
# ispell-local-dictionary: "british";
# end:
