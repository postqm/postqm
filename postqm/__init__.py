"""Postfix mail queue manager.

Postqm finds, reports on and processes entries in the Postfix mail
queue."""

__version__ = "96.0"


class Postqminternalerror(Exception):
    """Raised when Postqm finds itself in an invalid state."""


class Postqueueempty(Exception):
    """Raised when the mail queue is empty."""


# local variables:
# ispell-local-dictionary: "british";
# end:
