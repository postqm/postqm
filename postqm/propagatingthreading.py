"""Threading with result/exception propagation."""

import logging
import sys
from threading import Thread

logger = logging.getLogger(__name__)


class Propagatingthread(Thread):
    """Thread that propagates result/exception to ``join``.

    Exceptions are also logged.

    Override ``propagatingrun`` instead of ``run`` if you do not pass
    ``target`` to ``__init__``."""

    result = None
    excinfo = None

    def run(self):
        """Dispatch to target or ``propagatingrun``.

        Don't override this method, see ``propagatingrun``.

        The result or exception is recorded so ``join`` can propagate
        it back to the parent thread."""
        try:
            if callable(self._target):
                self.result = self._target(*self._args, **self._kwargs)
            else:
                self.result = self.propagatingrun()  # pylint: disable=assignment-from-no-return
        except Exception as err:  # pylint: disable=broad-except
            logger.error("""Caught %s: "%s" in %s (%s).""",
                         type(err).__name__, err, type(self).__name__, self.name)
            self.excinfo = sys.exc_info()

    def propagatingrun(self):  # noqa: D401
        """Called if no ``target`` was passed to ``__init__.

        Override this method when you would override ``run`` in
        ``Threading``."""

    def join(self, timeout=None):
        """Wait until the thread terminates.

        Defer to ``super`` and propagate the run result or exception
        if appropriate.

        If timeout is passed and the thread is still alive after
        ``super`` ``join`` None is returned."""
        super().join(timeout=timeout)
        if timeout is not None and self.is_alive():
            return None
        if self.excinfo is not None:
            etype, evalue, traceback = self.excinfo
            self.excinfo = None
            raise etype(evalue).with_traceback(traceback)
        return self.result


# local variables:
# ispell-local-dictionary: "british";
# end:
