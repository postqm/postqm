"""Postfix mail queue manager.

Postqm finds, reports on and processes entries in the Postfix mail
queue."""

# Copyright (C) 2007 - 2019 Ulrik Haugen
# Copyright (C) 2007 - 2024 Linköpings universitet

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import collections
import datetime
import logging
import os
import re
import signal
import sys
from argparse import (REMAINDER,
                      Action,
                      ArgumentError,
                      ArgumentParser,
                      RawDescriptionHelpFormatter)
from functools import reduce
from locale import getpreferredencoding
from operator import add
from os import EX_OK, EX_OSERR, EX_UNAVAILABLE, EX_USAGE
from pprint import pformat
from signal import SIGPIPE
from subprocess import PIPE, STDOUT, Popen

from pkg_resources import DistributionNotFound, get_distribution

from . import Postqueueempty
from .cachedproperty import cachedproperty
from .propagatingthreading import Propagatingthread
from .queuereader import postqueuerecords

__author__ = "Ulrik Haugen <qha@lysator.liu.se>"
try:
    __version__ = get_distribution('postqm').version
except DistributionNotFound:
    __version__ = "not installed"

preferredencoding = getpreferredencoding()

logger = logging.getLogger(__name__)

monthliterals = ('jan', 'feb', 'mar', 'apr', 'may', 'jun',
                 'jul', 'aug', 'sep', 'oct', 'nov', 'dec')
months = {monthliteral.casefold(): enumeration + 1
          for enumeration, monthliteral
          in enumerate(monthliterals)}

weekdayliterals = ('mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun')
weekdays = {weekdayliteral.casefold(): enumeration + 1
            for enumeration, weekdayliteral
            in enumerate(weekdayliterals)}


class Postqueueerror(Exception):
    """Raised when postqueue does not perform as expected."""


class Postcaterror(Exception):
    """Raised when postcat does not perform as expected."""


class Postsupererror(Exception):
    """Raised when postsuper does not perform as expected."""


class Malformedmessage(Exception):
    """Raised when Postqm doesn't know how to parse a message."""


class Filtermatcherror(Exception):
    """Raised when trying to store or evaluate an undefined filter."""


class Returncodes:
    """Mapping of return codes.

    To ``sysexits.h`` symbols and signal names."""

    sysexits = {getattr(os, name): name
                for name
                in dir(os)
                if name.startswith('EX_')}

    negatedsignals = {-getattr(signal, name): name
                      for name
                      in dir(signal)
                      if name.startswith('SIG')}

    returncodes = dict(list(sysexits.items()) + list(negatedsignals.items()))

    @classmethod
    def lookup(cls, returncode):
        """Return the name associated with *returncode*.

        Or *returncode* if no such name is defined."""
        return cls.returncodes.get(returncode, returncode)


class Stderraccumulator(Propagatingthread):
    """Thread to follow and accumulate ``stderr``.

    Of ``postcat``."""

    def __init__(self, proc):
        """Store ``procname`` and ``errstream``."""
        super().__init__(
            # Unceremoniously kill this thread if the main thread
            # exits first.
            daemon=True)
        if isinstance(proc.args, str):
            self.procname = proc.args.split()[0]
        else:
            self.procname = proc.args[0]
        self.errstream = proc.stderr

    def propagatingrun(self):
        """Accumulate and return errors in ``errstream``."""
        try:
            errlines = []
            for rawline in self.errstream:
                errlines.append(rawline.decode(preferredencoding,
                                               "backslashreplace").rstrip())
            return errlines
        finally:
            self.errstream.close()


class Qmessage:
    """This class represents a queue message read with ``postcat``."""

    def __init__(self, qid, readenvelope=True, readheaders=True,
                 readbody=False):
        """Initialise the ``Qmessage`` and record the desired content."""
        self._currentlyexpecting = 'marker'
        self.qid = qid
        self.readenvelope = readenvelope
        self.readheaders = readheaders
        self.readbody = readbody

        if readenvelope:
            self.envelope = {}

        if readheaders:
            self.headers = {}

        if readbody:
            self.body = []

        self.retrieveresult = self._retrievemessageinfo()

    def _retrievemessageinfo(self):
        """Retrieve information from ``postcat -q`` ``self.qid``."""
        try:
            catproc = Popen(['postcat', '-q', self.qid],
                            stdout=PIPE, stderr=PIPE)
            caterraccumulator = Stderraccumulator(catproc)
            caterraccumulator.start()

            try:
                # Rstrip both carriage return and line feed as
                # Qmessage can be composed on a platform with foreign
                # line separators.
                messagelines = (
                    messageline.decode(preferredencoding,
                                       "backslashreplace").rstrip("\r\n")
                    for messageline
                    in catproc.stdout)
                nextsection = self._readtomarker(messagelines)
                assert nextsection == "envelope"
                nextsection = self._consumeenvelope(messagelines)
                assert nextsection == "headers"
                nextsection = self._consumeheaders(messagelines)
                if self.readbody and nextsection == "body":
                    nextsection = self._consumebody(messagelines)
                assert nextsection in ("body", "footer")
            except (Postcaterror, Malformedmessage) as err:
                logger.warning(
                    "Caught error reading output from postcat -q %s: %s %s",
                    self.qid, type(err).__name__, err)
        finally:
            catproc.stdout.close()
            catproc.wait()
            caterr = caterraccumulator.join()

        if catproc.returncode not in (EX_OK, -SIGPIPE):
            reason = ""

            if (caterr == [f"postcat: fatal: open queue file {self.qid}:"
                           " No such file or directory"]):
                reason = ", news at eleven"
                logwith = logger.info
            else:
                reason = ", {}".format('\n'.join(caterr))
                logwith = logger.warning

            logwith("Got non ok return from postcat -q %s: %s%s",
                    self.qid, Returncodes.lookup(catproc.returncode), reason)
            return False
        return True

    @staticmethod
    def _readtomarker(instream):
        """Consume one line from *instream* to reach envelope records.

        Return name of next expected section."""
        messageline = next(instream, None)
        if messageline is None:
            raise Postcaterror("no output.")
        if messageline.startswith("*** ENVELOPE RECORDS"):
            return "envelope"
        raise Malformedmessage(
            f"output started with: {messageline}")

    def _consumeenvelope(self, instream):
        """Consume envelope records from *instream*.

        Return name of next expected section."""
        for messageline in instream:
            if messageline.startswith("*** MESSAGE CONTENTS"):
                return "headers"
            if not self.readenvelope:
                continue
            field, _sep, value = messageline.partition(": ")
            self.envelope.setdefault(field, []).append(value)
        raise Malformedmessage("no message contents.")

    def _consumeheaders(self, instream):
        """Consume headers from *instream*.

        Return name of next expected section."""
        currentheader = None
        for messageline in instream:
            if messageline == "":
                return "body"
            if messageline.startswith("*** HEADER EXTRACTED"):
                return "footer"
            if not self.readheaders:
                continue
            if messageline[0] in " \t":
                # Continuation line.
                if currentheader is None:
                    raise Malformedmessage(
                        f"Continuation line before first header: {messageline}")
                self.headers[currentheader][-1] += messageline[1:]
                continue
            if ":" not in messageline:
                raise Malformedmessage(
                    "Header line not matching start of header"
                    f"or continuation: {messageline}")
            field, _sep, value = messageline.partition(":")
            currentheader = field.casefold()
            if value != "" and value[0] in " \t":
                self.headers.setdefault(currentheader,
                                        []).append(value[1:])
            else:
                self.headers.setdefault(currentheader, []).append(value)
            continue
        raise Malformedmessage("no qentry footer")

    def _consumebody(self, instream):
        """Consume body from *instream*.

        Return name of next expected section."""
        for messageline in instream:
            if messageline.startswith("*** HEADER EXTRACTED"):
                return "footer"
            self.body.append(messageline)
        raise Malformedmessage("no qentry footer")

    def getheader(self, headername):
        """Return *headername* from message headers if available and loaded.

        None otherwise."""
        return self.headers.get(headername, None)


class Qentry:
    """This class represents a queue entry."""

    qidrx = re.compile('[0-9A-F]*')

    queuefields = ('qid', 'active', 'held', 'size',
                   'qdate', 'sender', 'recipient',
                   'recipients', 'relayresponse',
                   'one', 'qage',
                   'senderdomain', 'recipientdomain')
    singlevaluefields = ('qid', 'active', 'held', 'size',
                         'qdate', 'sender', 'recipients',
                         'one', 'qage', 'senderdomain')
    booleanfields = ('active', 'held')
    integerfields = ('recipients', 'size', 'one', 'qage')
    datefields = ('qdate',)

    one = 1

    @staticmethod
    def domainforaddress(address):
        """Return the domain part of email *address*.

        (See: Rfc 5321.)"""
        try:
            return address.rsplit("@", 1)[1]
        except IndexError:
            return None

    @cachedproperty
    def senderdomain(self):
        """Sender domain."""  # noqa: D401
        return self.domainforaddress(self.sender)

    @cachedproperty
    def recipientdomain(self):
        """Recipient domains."""  # noqa: D401
        return [self.domainforaddress(address)
                for address
                in self.recipient]

    @cachedproperty
    def qdate(self):
        """Parse ``rawqdate`` and return a ``date`` object.

        Provided the closest preceding date/month has the correct day
        of week, None otherwise.

        ``rawqdate`` is stored as **day of week** (three characters),
        **month** (three characters) and **date** (two digits)."""
        dow, mon, dd = self.rawqdate

        today = datetime.date.today()

        isoweekday = weekdays[dow.casefold()]
        month = months[mon.casefold()]
        day = int(dd)

        if (month > today.month
            or (month == today.month
                and day > today.day)):
            year = today.year - 1
        else:
            year = today.year

        date = datetime.date(year, month, day)

        if not date.isoweekday() == isoweekday:
            return None
        return date

    @cachedproperty
    def qage(self):
        """Age in days."""  # noqa: D401
        return (datetime.date.today() - self.qdate).days

    def __init__(self, rawentry):
        """Initialise attributes and dispatch to ``_parserawentry``."""
        self.headers = {}
        self.requestedheaders = set()

        self._parserawentry(rawentry)

    def _parserawentry(self, rawentry):
        """Parse an entry from ``postqueue -p`` output."""
        try:
            # Sometimes size is so great that there's no space between
            # qid and size for active and held entries, so get qid by
            # regex instead:
            self.qid = self.qidrx.match(rawentry[0]).group()

            flags = rawentry[0][len(self.qid):len(self.qid) + 1]
            self.active = flags.find("*") != -1
            self.held = flags.find("!") != -1

            fields = [field
                      for field
                      in rawentry[0][len(self.qid) + 1:].split(" ")
                      if field != ""]

            self.size = int(fields[0])
            self.rawqdate = fields[1:4]

            # envelope sender:
            self.sender = fields[-1].casefold()

            # envelope recipients:
            self.recipientbyresponse = collections.defaultdict(list)

            relayresponse = None
            for recipientorresponse in rawentry[1:]:
                recipientorresponse = recipientorresponse.strip()

                if (recipientorresponse.startswith("(")
                    and recipientorresponse.endswith(")")):
                    relayresponse = recipientorresponse[1:-1]
                else:
                    self.recipientbyresponse[relayresponse].append(
                        recipientorresponse.casefold())

            self.recipient = reduce(add,
                                    self.recipientbyresponse.values(),
                                    [])
            self.relayresponse = list(self.recipientbyresponse.keys())
            if None in self.relayresponse:
                self.relayresponse.remove(None)
            self.recipients = len(self.recipient)
        except ValueError:
            print("Encountered ValueError on"
                  f" postqueue -p line: `{rawentry}'")
            raise

    def getsequencefor(self, fieldname):
        """Return a sequence of values for *fieldname* or None."""
        if (fieldname in self.queuefields
            and fieldname in self.singlevaluefields):
            if getattr(self, fieldname) is None:
                return None
            return (getattr(self, fieldname),)
        if (fieldname in self.queuefields
            and fieldname not in self.singlevaluefields):
            return getattr(self, fieldname)
        return self.getheader(fieldname)

    def requestheaders(self, headernames):
        """Note that *headernames* will be needed at some later stage.

        The next call to ``loadheaders`` will try to get them."""
        self.requestedheaders.update(headernames)

    def getheader(self, headername):
        """Return *headername* from message headers.

        Message headers are read from ``postcat`` if not present
        already."""
        self.loadheaders()
        return self.headers[headername]

    def loadheaders(self):
        """Create a ``Qmessage`` and get ``requestedheaders`` from it."""
        if not self.requestedheaders:
            return True

        qmessage = Qmessage(self.qid, readenvelope=False)

        for headername in self.requestedheaders:
            self.headers[headername] = qmessage.getheader(headername)
        self.requestedheaders = set()

        return qmessage.retrieveresult

    def getoutputheader(self, verbose=False):
        """Return a header.

        Suitable for printing along with the result of ``__str__``."""
        headersoutputheader = ""
        if self.headers:
            for headername in self.headers:
                # pylint: disable-next=consider-using-f-string
                headersoutputheader += ("\n{}{}: {} (header)".format(
                    " " * 4,
                    headername,
                    headername.upper()))

        firstheaderline = (
            "--------QID  --SIZE  NUM  YYYY-MM-DD  SENDER (envelope)")
        restheaderlines = ""
        if verbose:
            restheaderlines = "\n    (RELAYRESPONSE)"
        restheaderlines += "\n    RECIPIENTS (envelope)"
        return ("-" * len(firstheaderline) + "\n"
                + firstheaderline
                + restheaderlines
                + headersoutputheader)

    def __str__(self):
        """Return a representation suitable for printing."""
        return self.tostring()

    def tostring(self, verbose=False):
        """Return a representation suitable for printing.

        If *verbose* is ``True`` ``relayresponse`` is included."""
        self.loadheaders()
        qmap = dict(self.__dict__)
        qmap['qdate'] = self.qdate
        qmap['qidwflags'] = (self.qid
                             + {True: "*", False: ""}[self.active]
                             + {True: "!", False: ""}[self.held])
        qmap['headers'] = ""
        if self.headers:
            for headername in self.headers:
                qmap['headers'] += ("{}{}: {}".format(
                    "\n" + " " * 4,
                    headername,
                    self.getheader(headername)))
        # recipients w/o response must be output first
        if verbose:
            indentedrecipients = []
            if None in self.recipientbyresponse:
                for recipient in sorted(self.recipientbyresponse[None]):
                    indentedrecipients.append(" " * 4 + recipient)

            for response, recipients in self.recipientbyresponse.items():
                if response is None:
                    break
                # pylint: disable-next=consider-using-f-string
                indentedrecipients.append("{}({})".format(
                    " " * 4, response))
                for recipient in sorted(recipients):
                    # pylint: disable-next=consider-using-f-string
                    indentedrecipients.append("{}{}".format(
                        " " * 4, recipient))
        else:
            indentedrecipients = (" " * 4 + recipient
                                  for recipient
                                  in sorted(self.recipient))

        qmap['indentedrecipients'] = "\n".join(indentedrecipients)

        return ("{qidwflags:<12} {size:6}"
                "  {recipients:3}  {qdate:%Y-%m-%d}"
                "  {sender}\n{indentedrecipients}{headers}".format(
                    **qmap))

    def dump(self):
        """Return a remotely readable representation of this queue entry."""
        self.loadheaders()
        return pformat(self.__dict__, indent=4)

    def __repr__(self):
        """Return a representation of this ``Qentry``."""
        return f"{type(self).__name__}({self.__dict__.__repr__()})"


class Entryfilter:
    """Representation of a filter.

    Once initialised calling the filter with a ``Qentry`` will test if
    the entry matches the filter."""

    def __init__(self, field, matchtype, value):
        """Record filter terms.

        Parameters *field*, *matchtype* and *value* are assumed to be
        in string form."""
        self.field = field.casefold()

        if self.field in Qentry.datefields:
            year, month, day = value.split("-")
            self.value = datetime.date(int(year), int(month), int(day))
        elif self.field in Qentry.booleanfields:
            self.value = {'true': True, 'false': False}[value.casefold()]
        elif self.field in Qentry.integerfields:
            self.value = int(value)

        if matchtype == 'rx':
            if hasattr(self, 'value'):
                raise Filtermatcherror(
                    f"rx match type not defined on {self.field} field")
            self.value = re.compile(value, re.M)

        if not hasattr(self, 'value'):
            self.value = value

        try:
            self.match = getattr(self, 'filter' + matchtype)
        except AttributeError:
            raise Filtermatcherror(
                f"unrecognised match type {matchtype}") from None

    def filterrx(self, entry):  # noqa: D400,D403
        """regular expression"""
        fieldvalues = entry.getsequencefor(self.field)
        return (fieldvalues
                and True in (fieldvalue is not None
                             and self.value.search(fieldvalue) is not None
                             for fieldvalue
                             in fieldvalues))

    def filtereq(self, entry):  # noqa: D400,D403
        """equality"""
        fieldvalues = entry.getsequencefor(self.field)
        return fieldvalues and self.value in fieldvalues

    def filterlt(self, entry):  # noqa: D400,D403
        """less than"""
        fieldvalues = entry.getsequencefor(self.field)
        return (not fieldvalues
                or True in (fieldvalue < self.value
                            for fieldvalue
                            in fieldvalues))

    def filtergt(self, entry):  # noqa: D400,D403
        """greater than"""
        fieldvalues = entry.getsequencefor(self.field)
        return (fieldvalues
                and True in (fieldvalue > self.value
                             for fieldvalue
                             in fieldvalues))

    def __call__(self, entry):
        """Defer to ``match`` on *self*."""
        return self.match(entry)


class Invertedentryfilter(Entryfilter):
    """Representation of an inverted filter."""

    def __call__(self, entry):
        """Defer to the underlying ``Entryfilter`` but invert the result."""
        return not super().__call__(entry)


class Entryfilterlist:
    """Representation of a filter list.

    Including a ``set`` of the headers needed to evaluate that
    list."""

    def __init__(self):
        """Initialise attributes."""
        self.filterlist = []
        self.headersneeded = set()

    def __len__(self):
        """Return the length of the filter list."""
        return len(self.filterlist)

    def addfilter(self, entryfilter):
        """Add a filter."""
        if entryfilter.field in Qentry.queuefields:
            self.filterlist.insert(
                0, entryfilter)
        else:
            # These filters need headers, so if another filter can
            # disqualify an entry first - let it - that way we don't
            # need to call postcat for that entry.
            self.filterlist.append(
                entryfilter)
            self.headersneeded.add(entryfilter.field)

    def evalfilters(self, entry):
        """Check if *entry* matches the filter list."""
        for match in self.filterlist:
            if not match(entry):
                return False
        return True


class Addfilteraction(Action):
    """Action to store filter entries in a filter list."""

    efclassbytype = {'include': Entryfilter,
                     'exclude': Invertedentryfilter}

    def __init__(self, option_strings, dest,
                 nargs=None, const=None, default=None,
                 **kwargs):
        """Check parameters and initialise add filter action.

        Pass 'include' or 'exclude' as ``const`` to ``add_argument``
        with this class as ``action``."""
        if nargs != 3:
            raise ValueError("nargs must be 3")
        if not isinstance(default, Entryfilterlist):
            raise ValueError("default type must be Entryfilterlist")
        self.efclass = self.efclassbytype[const]
        super().__init__(option_strings, dest,
                         nargs=nargs, const=const,
                         default=default,
                         **kwargs)

    def __call__(self, parser, namespace, values,
                 option_string=None):
        """Add entry filter defined by *values* to filter list."""
        try:
            getattr(namespace, self.dest).addfilter(self.efclass(*values))
        except Filtermatcherror as err:
            raise ArgumentError(self, err) from err


class Stderrlogger(Propagatingthread):
    """Thread to follow and log ``stderr``.

    Of ``postqueue``."""

    def __init__(self, proc):
        """Store ``procname`` and ``errstream``."""
        super().__init__(
            # Unceremoniously kill this thread if the main thread
            # exits first.
            daemon=True)
        if isinstance(proc.args, str):
            self.procname = proc.args.split()[0]
        else:
            self.procname = proc.args[0]
        self.errstream = proc.stderr

    def propagatingrun(self):
        """Warn about errors in ``errstream``."""
        try:
            for rawline in self.errstream:
                line = rawline.decode(preferredencoding,
                                      "backslashreplace").rstrip()
                logger.error("%s stderr: %s", self.procname, line)
        finally:
            self.errstream.close()


class Qidfeeder(Propagatingthread):
    """Thread to feed qids to ``postsuper`` ``stdin``."""

    # Record of qids written to postsuper stdin.
    qidrecord = None

    def __init__(self, proc, qidsource, name=None):
        """Store ``procname``, ``instream`` and *qidsource*."""
        super().__init__(
            name=name,
            # Unceremoniously kill this thread if the main thread
            # exits first.
            daemon=True)
        if isinstance(proc.args, str):
            self.procname = proc.args.split()[0]
        else:
            self.procname = proc.args[0]
        self.instream = proc.stdin
        self.qidsource = qidsource

    def propagatingrun(self):
        """Write qids to ``instream``."""
        self.qidrecord = set()
        try:
            for qid in self.qidsource:
                self.instream.write(f"{qid}\n".encode(preferredencoding))
                self.qidrecord.add(qid)
        finally:
            self.instream.close()


def runpostqueue():
    """Yield lines from ``postqueue -p`` ``stdout``."""
    try:
        qproc = Popen(['postqueue', '-p'], stdout=PIPE, stderr=PIPE)
        # Keep reading stderr from postqueue.
        postqueueerrlogger = Stderrlogger(qproc)
        postqueueerrlogger.start()

        for outline in qproc.stdout:
            yield outline.decode(preferredencoding,
                                 "backslashreplace").strip()
    finally:
        qproc.wait()
        postqueueerrlogger.join()

    if qproc.returncode != EX_OK:
        # pylint: disable=consider-using-f-string
        raise Postqueueerror(
            "Got non ok return from 'postqueue -p': {}".format(
                Returncodes.lookup(qproc.returncode)))


def readpostqueue(entryfilterlist, extraheadersneeded=None):
    """Yield ``postqueue`` entries that match all filters."""
    if extraheadersneeded is None:
        extraheadersneeded = set()
    headersneeded = entryfilterlist.headersneeded | extraheadersneeded

    qrecords = postqueuerecords(runpostqueue())

    for qrecord in qrecords:
        qentry = Qentry(qrecord)
        qentry.requestheaders(headersneeded)
        if entryfilterlist.evalfilters(qentry):
            yield qentry


def postqqids(postq, onlyheld=False):
    """Yield qid for each ``Qentry`` in *postq*.

    If *onlyheld* is true skip and print warnings about entries that
    are not held."""
    for entry in postq:
        if onlyheld and not entry.held:
            print(f"{entry.qid} not held, skipped.")
            continue
        yield entry.qid


def applyqoperation(command, operandqids,
                    qidexpectedprefix, qidexpectedsuffix,
                    commandexpectedoutputfmt,
                    diagnosticpresenttense,
                    diagnosticpasttense,
                    diagnosticpasttenseterminated):
    """Feed qids to command.

    *command* is a sequence fit to be passed to ``Popen``.

    *operandqids* is an iterable of qids to be fed to the stdin of
    *command*.

    *qidexpectedprefix* and *qidexpectedsuffix* are what should
    surround the qid on each output line from command pertaining to a
    successful operation.

    *commandexpectedoutputfmt* is what the summary should look like
    after formatting with the number of successful operations for the
    first replacement field and possibly a plural s for the second
    field.

    *diagnosticpresenttense*, *diagnosticpasttense* and
    *diagnosticpasttenseterminated* are the verbs to be used in
    diagnostic messages."""
    try:
        cmdproc = Popen(command, stdin=PIPE, stdout=PIPE, stderr=STDOUT)
        # Keep feeding operandqids to postsuper.
        postsuperqidfeeder = Qidfeeder(cmdproc, operandqids)
        postsuperqidfeeder.start()

        qidsprocessed = set()
        unmatchedcmdout = []

        for rawline in cmdproc.stdout:
            outline = rawline.decode(preferredencoding,
                                     "backslashreplace").rstrip()
            if (outline.startswith(qidexpectedprefix)
                and outline.endswith(qidexpectedsuffix)):
                qidsprocessed.add(
                    outline[len(qidexpectedprefix):-len(qidexpectedsuffix)])
            else:
                unmatchedcmdout.append(outline)
    finally:
        postsuperqidfeeder.join()
        cmdproc.wait()
        cmdproc.stdout.close()

    expectedoutput = commandexpectedoutputfmt.format(
        len(qidsprocessed),
        ["", "s"][len(qidsprocessed) > 1])

    if unmatchedcmdout and unmatchedcmdout[-1] == expectedoutput:
        del unmatchedcmdout[-1]

    cmdstat = cmdproc.returncode

    if cmdstat != EX_OK:
        print(f"{command[0]} status: {Returncodes.lookup(cmdstat)}")

    if unmatchedcmdout:
        print("{} unexpected output:\n{}".format(
            command[0], "\n".join(unmatchedcmdout)))

    qidsrequested = postsuperqidfeeder.qidrecord
    if qidsrequested - qidsprocessed == set():
        print(
            f"{len(qidsprocessed):d} messages {diagnosticpasttenseterminated}")
    else:
        # pylint: disable-next=consider-using-f-string
        print("could not {} {}".format(diagnosticpresenttense,
                                       ", ".join(
                                           qidsrequested - qidsprocessed)))
        print(f"{len(qidsprocessed):d}/{len(qidsrequested):d}"
              f" messages {diagnosticpasttenseterminated}")

    if qidsprocessed - qidsrequested != set():
        # pylint: disable=consider-using-f-string
        print("unexpectedly {} {}".format(
            diagnosticpasttense,
            ", ".join(qidsprocessed - qidsrequested)))
        raise Postsupererror(
            "Unexpectedly {} {:d} messages".format(
                diagnosticpasttense, len(qidsprocessed - qidsrequested)))


def cmdls(globalargs):
    """List messages."""
    parser = ArgumentParser(
        usage="%(prog)s ls [options] [field [field ...]]",
        description="List messages.")
    parser.add_argument('--dump', action='store_true',
                        help="print internal data structures")
    parser.add_argument('-v', '--verbose', action='store_true',
                        help="include relayresponse in output")
    parser.add_argument('field', nargs='*',
                        help="header field to include in the listing")
    args = parser.parse_args(globalargs.commandargs)

    headerfields = set()

    for field in args.field:
        fieldname = field.casefold()
        if fieldname not in Qentry.queuefields:
            headerfields.add(fieldname)

    qentry = None
    entryid = 0
    try:
        postq = readpostqueue(globalargs.entryfilterlist, headerfields)

        for entryid, qentry in enumerate(postq, start=1):
            if args.dump:
                print(qentry.dump())
            elif args.verbose:
                print(qentry.tostring(verbose=True))
            else:
                print(qentry)
    except Postqueueerror as err:
        logger.error(err)
        return EX_UNAVAILABLE
    except Postqueueempty as err:
        print(err, file=sys.stderr)
        return EX_OK

    if qentry is not None:
        # No need to load headers in qentry as it was just printed.
        print(qentry.getoutputheader(args.verbose))

        print(f"Listed {entryid:d} messages.")
    else:
        print("No matching messages.")
    return EX_OK


def cmdtop(globalargs):
    """Top lists of messages."""
    parser = ArgumentParser(
        usage="%(prog)s top [options]"
        " [field for grouping [field to measure]]",
        description="""Top lists of messages.

With one argument list occurrences of the selected field by frequency.
Only fields with frequency greater than 2 are listed.

The two argument form allows measurement of other fields. For example,
to see the average age of messages by recipient you would say:

       %(prog)s top recipient qage

Field to measure must be an integer field.

When you specify a field to measure the default reduce method becomes
average instead of sum. If you still want a sum for the field to
measure you may specify the reduce method with options. For example,
to see what senders are consuming the most queue space you would say:

       %(prog)s top --sum sender size""",
        epilog="For information about available fields:"
        " see %(prog)s --help.",
        formatter_class=RawDescriptionHelpFormatter)
    reducemethod = parser.add_argument_group("reduce method")
    reducemethod.add_argument('--sum', action='store_const',
                              dest='reducemethod', const='sum',
                              help="reduce collected measurements by"
                              " adding them")
    reducemethod.add_argument('--average', action='store_const',
                              dest='reducemethod', const='average',
                              help="reduce collected measurements by"
                              " averaging them")
    fields = parser.add_argument_group("fields")
    fields.add_argument('fieldforgrouping', metavar="[field for grouping]",
                        nargs='?', default='subject',
                        help="field to group queue entries by"
                        " (default: subject)")
    fields.add_argument('fieldtomeasure', metavar="[field to measure]",
                        nargs='?', default='one',
                        choices=Qentry.integerfields,
                        help="field to measure on each queue entry"
                        " (default: frequency, no field)")

    args = parser.parse_args(globalargs.commandargs)
    fieldforgrouping = args.fieldforgrouping.casefold()
    fieldtomeasure = args.fieldtomeasure.casefold()

    if args.reducemethod is None:
        if fieldtomeasure == 'one':
            args.reducemethod = 'sum'
        else:
            args.reducemethod = 'average'

    headersneeded = set()
    if fieldforgrouping not in Qentry.queuefields:
        headersneeded.add(fieldforgrouping)
    if fieldtomeasure not in Qentry.queuefields:
        headersneeded.add(fieldtomeasure)
    topby = fieldforgrouping.upper()
    topof = ""
    if fieldtomeasure == 'one' and args.reducemethod == 'sum':
        topof = "FREQ"
    else:
        topof = f"{args.reducemethod.upper()}({fieldtomeasure.upper()})"

    # Values are tuples of frequency and sum of measurements
    resultbygroup = collections.defaultdict(lambda: (0, 0))

    def addmeasurement(entry, measurement):
        """Return incremented *entry*.

        Frequency is incremented by one and sum is incremented by
        *measurement*."""
        return (entry[0] + 1, entry[1] + measurement)

    try:
        postq = readpostqueue(globalargs.entryfilterlist, headersneeded)

        for entry in postq:
            group = entry.getsequencefor(args.fieldforgrouping)
            # fieldtomeasure must be an integer field, so don't
            # bother with possible futher values:
            measurement = entry.getsequencefor(args.fieldtomeasure)
            if measurement:
                if not group:
                    resultbygroup[None] = addmeasurement(
                        resultbygroup[None], measurement[0])
                else:
                    for attr in group:
                        resultbygroup[attr] = addmeasurement(
                            resultbygroup[attr], measurement[0])
    except Postqueueerror as err:
        logger.error(err)
        return EX_UNAVAILABLE
    except Postqueueempty as err:
        print(err, file=sys.stderr)
        return EX_OK

    if not resultbygroup:
        print("No matching fields found.")
        return EX_OK

    top = []
    for attr, (frequency, measurement) in resultbygroup.items():
        if frequency > 1:
            if args.reducemethod == 'sum':
                top.append((measurement, attr))
            elif args.reducemethod == 'average':
                top.append((measurement / frequency, attr))

    if not top:
        print("No matching fields with frequency greater than 2 found.")
        return EX_OK

    top.sort()
    fs = "{:{topoflen:d}} {}"
    topoflen = len(topof)
    for reducedmeasure, attr in top:
        print(fs.format(reducedmeasure, attr, topoflen=topoflen))
    print(fs.format("-" * topoflen, "-" * len(topby), topoflen=topoflen))
    print(fs.format(topof, topby, topoflen=topoflen))
    return EX_OK


def cmdrm(globalargs):
    """Remove held messages."""
    parser = ArgumentParser(
        usage="%(prog)s rm [options]",
        description="Remove held messages."
        " See: http://www.homestarrunner.com/sbemail20.html")
    parser.add_argument('-n', '--dry-run', action='store_true',
                        help="do not actually delete messages")
    parser.add_argument('-f', '--force', action='store_true',
                        help="proceed when messages that were"
                        " not held are selected")
    args = parser.parse_args(globalargs.commandargs)

    if not globalargs.entryfilterlist.filterlist:
        dryrunlabel = {True: " (dry run)",
                       False: ""}[args.dry_run]
        answer = input(
            f"This would delete the entire mail queue{dryrunlabel}."
            "\nConfirm [y/n]: ")
        if answer != 'y':
            return EX_USAGE

    try:
        postq = readpostqueue(globalargs.entryfilterlist)

        if args.dry_run:
            print(f"Would have requested {len(list(postq)):d}"
                  " messages to be DELETED!!")
            return EX_OK

        command = ['postsuper', '-d', '-']
        if not args.force:
            # Marginally reduce the risk of races (belt and braces):
            command.append('hold')

        applyqoperation(command,
                        postqqids(postq, onlyheld=not args.force),
                        "postsuper: ", ": removed",
                        "postsuper: Deleted: {} message{}",
                        "delete", "deleted", "DELETED!!")
    except Postqueueerror as err:
        logger.error(err)
        return EX_UNAVAILABLE
    except Postsupererror as err:
        logger.error(err)
        return EX_OSERR
    except Postqueueempty as err:
        print(err, file=sys.stderr)
        return EX_OK
    return EX_OK


def cmdhold(globalargs):
    """Hold messages."""
    parser = ArgumentParser(usage="%(prog)s hold [options]",
                            description="Hold messages.")
    parser.add_argument('-n', '--dry-run', action='store_true',
                        help="do not actually hold messages")
    args = parser.parse_args(args=globalargs.commandargs)

    try:
        postq = readpostqueue(globalargs.entryfilterlist)

        if args.dry_run:
            print(f"Would have requested {len(list(postq)):d}"
                  " messages to be held")
            return EX_OK

        applyqoperation(['postsuper', '-h', '-'],
                        postqqids(postq),
                        "postsuper: ", ": placed on hold",
                        "postsuper: Placed on hold: {} message{}",
                        "hold", "held", "held.")
    except Postqueueerror as err:
        logger.error(err)
        return EX_UNAVAILABLE
    except Postsupererror as err:
        logger.error(err)
        return EX_OSERR
    except Postqueueempty as err:
        print(err, file=sys.stderr)
        return EX_OK
    return EX_OK


def cmdrelease(globalargs):
    """Release messages."""
    parser = ArgumentParser(
        usage="%(prog)s release [options]",
        description="Release messages from hold. You should only release"
        " messages that have been queued a small fraction of"
        " maximal_queue_lifetime and bounce_queue_lifetime, (see"
        " postconf(5) and postsuper(1)); use requeue for older messages"
        " to keep them from bouncing if the first delivery attempt fails.")
    parser.add_argument('-n', '--dry-run', action='store_true',
                        help="do not actually release messages")
    args = parser.parse_args(args=globalargs.commandargs)

    try:
        postq = readpostqueue(globalargs.entryfilterlist)

        if args.dry_run:
            print(f"Would have requested {len(list(postq)):d}"
                  " messages to be released")
            return EX_OK

        applyqoperation(['postsuper', '-H', '-'],
                        postqqids(postq),
                        "postsuper: ", ": released from hold",
                        "postsuper: Released from hold: {} message{}",
                        "release", "released", "released.")
    except Postqueueerror as err:
        logger.error(err)
        return EX_UNAVAILABLE
    except Postsupererror as err:
        logger.error(err)
        return EX_OSERR
    except Postqueueempty as err:
        print(err, file=sys.stderr)
        return EX_OK
    return EX_OK


def cmdrequeue(globalargs):
    """Requeue messages."""
    parser = ArgumentParser(usage="%(prog)s requeue [options]",
                            description="Requeue messages.")
    parser.add_argument('-n', '--dry-run', action='store_true',
                        help="do not actually requeue messages")
    args = parser.parse_args(args=globalargs.commandargs)

    try:
        postq = readpostqueue(globalargs.entryfilterlist)

        if args.dry_run:
            print(f"Would have requested {len(list(postq)):d}"
                  " messages to be requeued")
            return EX_OK

        applyqoperation(['postsuper', '-r', '-'],
                        postqqids(postq),
                        "postsuper: ", ": requeued",
                        "postsuper: Requeued: {} message{}",
                        "requeue", "requeued", "requeued.")
    except Postqueueerror as err:
        logger.error(err)
        return EX_UNAVAILABLE
    except Postsupererror as err:
        logger.error(err)
        return EX_OSERR
    except Postqueueempty as err:
        print(err, file=sys.stderr)
        return EX_OK
    return EX_OK


# No cmdactivate implemented to schedule immediate delivery via
# postqueue -i as it will not read qids from stdin.


def commandtable():
    """Return mapping from command name to implementing function."""
    commandprefix = 'cmd'
    return {command[len(commandprefix):]: globals()[command]
            for command
            in globals()
            if command.startswith(commandprefix)}


def parseargs(arguments):
    """Parse *arguments*."""
    usagesummary = """%(prog)s [filters] command [command arguments]
       %(prog)s [options]"""

    descriptionfmt = """Commands available:
{}

Match types available for filters:
* eq - equality
* gt - greater than
* lt - less than
* rx - regular expression

Fields available:
* qid - string
* active - boolean
* held - boolean
* size - integer
* qdate - date
* qage - integer
* sender - string
* senderdomain - string
* recipient - strings
* recipientdomain - strings
* recipients - integer
* relayresponse - strings

Also, at the cost of calling postcat for each message you can use the
names of headers."""
    commanddescriptionfmt = "* {} - {}"
    description = descriptionfmt.format(
        "\n".join(
            commanddescriptionfmt.format(
                cmdname, cmd.__doc__.lower().rstrip('.'))
            for cmdname, cmd
            in sorted(commandtable().items())))

    parser = ArgumentParser(usage=usagesummary,
                            description=description,
                            formatter_class=RawDescriptionHelpFormatter)
    parser.add_argument('--version', action='version',
                        version=f"Postqm {__version__}")

    filters = parser.add_argument_group("filters")

    filters.add_argument('-i', action=Addfilteraction,
                         const='include', nargs=3,
                         default=Entryfilterlist(), dest='entryfilterlist',
                         metavar=("field", "matchtype", "value"),
                         help="include entries where field matches value"
                         " with matchtype")
    filters.add_argument('-e', action=Addfilteraction,
                         const='exclude', nargs=3,
                         default=Entryfilterlist(), dest='entryfilterlist',
                         metavar=("field", "matchtype", "value"),
                         help="exclude entries where field matches value"
                         " with matchtype")

    parser.add_argument('command', choices=commandtable(),
                        metavar="command", help="command to call")
    parser.add_argument('commandargs', nargs=REMAINDER,
                        metavar="[command arguments]",
                        help="see %(prog)s command --help")

    return parser.parse_args(arguments)


def main():
    """Dispatch to ``logging``, ``parseargs``, and selected command."""
    logging.basicConfig(format="%(name)s %(levelname)s: %(message)s",
                        stream=sys.stderr,
                        level=logging.INFO)

    args = parseargs(sys.argv[1:])

    return commandtable()[args.command](args)


# local variables:
# ispell-local-dictionary: "british";
# end:
