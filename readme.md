# Postfix mail queue manager

Postqm finds, reports on and processes entries in the Postfix mail
queue.


## Development

To try Postqm on a real system without installing run flit like so:

    flit install --deps none --symlink --user

... and call postqm as:

    ~/.local/bin/postqm

Run tests with:

    python3 -m pytest

Run linters with:

    python3 -m pylint postqm tests
    python3 -m flake8 postqm tests
    python3 -m pydocstyle postqm tests
    python3 -m isort --check-only --diff postqm tests


## Author

Written by Ulrik Haugen.


## Other credits

Thanks to [Lysator acs](https://www.lysator.liu.se) for providing hosting
for this project!

Thanks to [Ctrl-c](https://ctrl-c.liu.se/) for providing the Gitlab ci
runner for this project!


# Housekeeping

<!--- local variables: -->
<!--- eval: (auto-fill-mode 1); -->
<!--- eval: (flyspell-mode 1); -->
<!--- ispell-local-dictionary: "british"; -->
<!--- end: -->
